-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 11-07-2018 a las 03:40:48
-- Versión del servidor: 5.6.20
-- Versión de PHP: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `oasis`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `courses`
--

CREATE TABLE IF NOT EXISTS `courses` (
`id` smallint(6) NOT NULL,
  `name` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `course_category` smallint(6) NOT NULL,
  `standar_price` decimal(10,0) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `courses`
--

INSERT INTO `courses` (`id`, `name`, `course_category`, `standar_price`, `created_at`, `updated_at`) VALUES
(1, 'Piano Básico', 1, '70000', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `course_categories`
--

CREATE TABLE IF NOT EXISTS `course_categories` (
`id` smallint(6) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Volcado de datos para la tabla `course_categories`
--

INSERT INTO `course_categories` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Piano', NULL, NULL),
(2, 'Guitarra', NULL, NULL),
(3, 'Saxofon', NULL, NULL),
(4, 'Vocalizacion', NULL, NULL),
(5, 'Bajo', NULL, NULL),
(6, 'Bateria', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `enrolments`
--

CREATE TABLE IF NOT EXISTS `enrolments` (
`id` int(10) unsigned NOT NULL,
  `student` int(11) NOT NULL,
  `course` smallint(6) NOT NULL,
  `payment_status` smallint(6) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `enrolment_statuses`
--

CREATE TABLE IF NOT EXISTS `enrolment_statuses` (
  `id` tinyint(4) NOT NULL,
  `name` varchar(20) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_statuses`
--

CREATE TABLE IF NOT EXISTS `user_statuses` (
`id` smallint(6) NOT NULL,
  `name` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `user_statuses`
--

INSERT INTO `user_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'activo', NULL, NULL),
(2, 'anulado', NULL, NULL);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `courses`
--
ALTER TABLE `courses`
 ADD PRIMARY KEY (`id`), ADD KEY `course_category` (`course_category`);

--
-- Indices de la tabla `course_categories`
--
ALTER TABLE `course_categories`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `enrolments`
--
ALTER TABLE `enrolments`
 ADD PRIMARY KEY (`id`), ADD KEY `payment_status` (`payment_status`), ADD KEY `student` (`student`), ADD KEY `course` (`course`);

--
-- Indices de la tabla `user_statuses`
--
ALTER TABLE `user_statuses`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `courses`
--
ALTER TABLE `courses`
MODIFY `id` smallint(6) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `course_categories`
--
ALTER TABLE `course_categories`
MODIFY `id` smallint(6) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `enrolments`
--
ALTER TABLE `enrolments`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `user_statuses`
--
ALTER TABLE `user_statuses`
MODIFY `id` smallint(6) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `courses`
--
ALTER TABLE `courses`
ADD CONSTRAINT `courses_ibfk_1` FOREIGN KEY (`course_category`) REFERENCES `course_categories` (`id`);

--
-- Filtros para la tabla `enrolments`
--
ALTER TABLE `enrolments`
ADD CONSTRAINT `enrolments_ibfk_1` FOREIGN KEY (`payment_status`) REFERENCES `payment_satuses` (`id`),
ADD CONSTRAINT `enrolments_ibfk_2` FOREIGN KEY (`student`) REFERENCES `students` (`id`),
ADD CONSTRAINT `enrolments_ibfk_3` FOREIGN KEY (`course`) REFERENCES `courses` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
