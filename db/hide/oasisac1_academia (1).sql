-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 10-01-2019 a las 11:52:25
-- Versión del servidor: 10.2.16-MariaDB
-- Versión de PHP: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `oasisac1_academia`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `courses`
--

CREATE TABLE `courses` (
  `id` smallint(6) NOT NULL,
  `name` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `course_category` smallint(6) NOT NULL,
  `standar_price` decimal(10,0) NOT NULL,
  `level` smallint(6) NOT NULL,
  `shedule` smallint(6) NOT NULL,
  `teacher` bigint(20) NOT NULL,
  `plan` smallint(6) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `courses`
--

INSERT INTO `courses` (`id`, `name`, `course_category`, `standar_price`, `level`, `shedule`, `teacher`, `plan`, `created_at`, `updated_at`) VALUES
(1, 'Piano Básico', 1, '80000', 1, 1, 1, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `course_categories`
--

CREATE TABLE `course_categories` (
  `id` smallint(6) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `course_categories`
--

INSERT INTO `course_categories` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Piano', NULL, NULL),
(2, 'Guitarra', NULL, NULL),
(3, 'Saxofon', NULL, NULL),
(4, 'Vocalizacion', NULL, NULL),
(5, 'Bajo', NULL, NULL),
(6, 'Bateria', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `days`
--

CREATE TABLE `days` (
  `id` smallint(6) NOT NULL,
  `day` varchar(15) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `days`
--

INSERT INTO `days` (`id`, `day`) VALUES
(1, 'Lunes'),
(2, 'Martes'),
(3, 'Miércoles'),
(4, 'Jueves'),
(5, 'Viernes'),
(6, 'Sábado');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `enrolments`
--

CREATE TABLE `enrolments` (
  `id` int(10) UNSIGNED NOT NULL,
  `student` int(11) NOT NULL,
  `course` smallint(6) NOT NULL,
  `payment_status` smallint(6) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `enrolment_statuses`
--

CREATE TABLE `enrolment_statuses` (
  `id` tinyint(4) NOT NULL,
  `name` varchar(20) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `enrolment_statuses`
--

INSERT INTO `enrolment_statuses` (`id`, `name`) VALUES
(1, 'Preinscrito'),
(2, 'Matriculado'),
(3, 'Inactivo'),
(4, 'Retirado');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `forms`
--

CREATE TABLE `forms` (
  `id` int(10) NOT NULL,
  `module` int(11) NOT NULL,
  `route` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `model` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `table_label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fields` text COLLATE utf8_unicode_ci NOT NULL,
  `create_fields` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `id_types`
--

CREATE TABLE `id_types` (
  `id` smallint(6) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `id_types`
--

INSERT INTO `id_types` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Tarjeta de identidad', NULL, NULL),
(2, 'Cédula de ciudadanía', NULL, NULL),
(3, 'Pasaporte', NULL, NULL),
(4, 'Cédula de extranjería', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `levels`
--

CREATE TABLE `levels` (
  `id` int(11) NOT NULL,
  `name` varchar(20) CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `levels`
--

INSERT INTO `levels` (`id`, `name`) VALUES
(1, 'Básico'),
(2, 'Intermedio'),
(3, 'Avanzado');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2016_10_28_163646_create_permitions_table', 2),
('2016_10_28_173206_create_modules_table', 2),
('2016_10_28_200220_create_privileges_table', 2),
('2017_06_16_170016_create_roles_table', 3),
('2017_02_18_161910_create_forms_table', 4),
('2017_06_21_053819_create_students_table', 5),
('2017_06_21_071154_create_id_types_table', 6);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `modules`
--

CREATE TABLE `modules` (
  `id` int(10) NOT NULL,
  `title` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `modules`
--

INSERT INTO `modules` (`id`, `title`, `name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'administración', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `people`
--

CREATE TABLE `people` (
  `id` int(10) NOT NULL,
  `name` varchar(39) DEFAULT NULL,
  `lastname` varchar(20) DEFAULT NULL,
  `id_type` smallint(6) DEFAULT NULL,
  `identification` bigint(20) DEFAULT NULL,
  `birthdate` varchar(10) DEFAULT NULL,
  `main_phone` bigint(10) DEFAULT NULL,
  `sec_phone` bigint(10) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `observations` varchar(67) DEFAULT NULL,
  `created_at` varchar(19) DEFAULT NULL,
  `updated_at` varchar(19) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `people`
--

INSERT INTO `people` (`id`, `name`, `lastname`, `id_type`, `identification`, `birthdate`, `main_phone`, `sec_phone`, `email`, `observations`, `created_at`, `updated_at`) VALUES
(2, 'Luis felipe', 'Sǭez aparicio', 2, 1067916515, '0000-00-00', 3207944050, 3145730610, 'pipesaez2@hotmail.com', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'Victor Daniel', 'Negrete Baza', 1, 1067865824, '0000-00-00', 3045889142, 3128934257, 'negretebazavictordaniel@gmail.com', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'lorena', 'vergara', 2, 1102809038, '0000-00-00', 3042132480, 0, 'loremaver@gmail.com', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'Isabel', 'Bulasco ', 1, 103577865, '0000-00-00', 3017787102, 7860657, 'Isabel020706@hotmail.com', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 'pedro luis', 'trujillo rios ', 2, 1233343079, '0000-00-00', 3116105143, 347953927, 'wplay123@outlook.com', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 'Jhan Carlos', 'Viloria Murillo', 2, 1007781249, '0000-00-00', 3215012831, 3215649925, 'jhanviloria45@gmail.com', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 'Jonathan', 'Dix Polo', 1, 1002998196, '0000-00-00', 7866062, 0, 'jonathandix12@gmail.com', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 'juan sebastian ', 'ballen manosalva', 2, 1116807689, '0000-00-00', 3184677781, 3165462227, 'sebastianballenm@hotmail.com', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, 'Daniel de jesus', 'Holguin Ricardo', 2, 1063294830, '0000-00-00', 3122493144, 3103753884, 'daniel.hr1@hotmail.com', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, 'Marcela', 'Martinez Babilonia', 2, 1007339981, '0000-00-00', 3126010564, 0, 'babiloniatheblackrose@gmail.com', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(14, 'Luciano ', 'Ferreras', 1, 37369943, '0000-00-00', 5, 0, 'lucianoferrerasmdh@gmail.com', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, 'Isaid joseth', 'Martinez martinez', 2, 1102840883, '0000-00-00', 3216515130, 0, 'isaidmarti7@hotmail.com', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(16, 'Magnoria', 'Gomez Plata', 2, 37948782, '0000-00-00', 3145639249, 0, 'margy-08@hotmail.com', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(18, 'MoisǸs David', 'Guzmǭn Pretel', 2, 1067938918, '0000-00-00', 3013663186, 3013663186, 'moisesdavid0010@gmail.com', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(19, 'Yury Estela', 'Sanchez Calderon', 2, 1067916327, '0000-00-00', 3005985881, 3008865408, 'tete.sanchez22@hotmail.com', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(20, 'Julio Antonio', 'Peniche Maestre', 2, 1069490233, '0000-00-00', 3043394463, 3135537781, 'julioantonio_06@hotmail.com', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(21, 'Juan JosǸ', 'Ely Romero', 2, 1068587999, '0000-00-00', 3135537781, 3043394463, 'jjelima@gmail.com', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(22, 'CLAUDIA MARIA ', 'DICKSON ROMERO', 2, 1067867929, '0000-00-00', 3218925019, 7894408, 'claudickr@hotmail.com', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(23, 'CESAR', 'GONZALEZ', 2, 1002161904, '0000-00-00', 3003419824, 0, 'cesar30992010@gmail.com', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(24, 'Mariluz', 'Alavarez Correa', 2, 1063146540, '0000-00-00', 3205376551, 3127909435, 'mariluzalvarezcorrea@hotmail.com', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(25, 'Leandra', 'Medina', 1, 1003433895, '0000-00-00', 3106392296, 0, 'leandramedina0707@gmail.com', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(26, 'M??nica', 'Ram??rez Ram??rez', 2, 1041234851, '0000-00-00', 3147378428, 3147378428, 'monimar1999@hotmail.com', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(27, 'Roberto Jose', 'Saenz Avilez', 2, 11156367, '0000-00-00', 3135859575, 0, 'ingrobertosaenz@gmail.com', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(28, 'Victor Hugo', 'Gomez Hernanez', 2, 16070977, '0000-00-00', 3128432909, 0, 'victorgomezmanizales@yahoo.com', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(29, 'Kevin alexander', 'londo??o asprilla', 2, 1020456429, '0000-00-00', 3045374799, 3045374799, 'kevin6londono@hotmail.com', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(30, 'Jesǧs Daniel', 'Narvǭez Mercado', 1, 1003069818, '0000-00-00', 3013275943, 3023025593, 'dajename26@hotmail.com', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(31, 'Santiago ', 'Nu??ez ', 1, 1003432142, '0000-00-00', 3107417987, 0, 'santiagonunez26@icloud.com', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(32, 'LUIS FERNANDO', 'CABALLERO MORALES', 2, 1068819539, '0000-00-00', 3114000749, 0, 'luis_caballero23@hotmail.es', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(33, 'Julio cesar', 'Argumedo hoyos', 2, 1067958072, '0000-00-00', 3215360021, 0, 'argumedojuliocesar53@gmail.com', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(34, 'Vanessa', 'blanco', 1, 1000412991, '0000-00-00', 3103646569, 0, 'Vanessacalderon34blanco@gmail.com', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(35, 'Luis Fernando', 'Giraldo Diaz', 2, 1002998887, '0000-00-00', 3015800591, 3126109060, 'yiraldfeer2014@gmail.com', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(36, 'Juan Pablo', 'Vidal Atencio', 2, 1067958988, '0000-00-00', 3216824537, 3135092269, 'juanpa_0703@hotmail.com', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(37, 'Lina Marcela', 'GutiǸrrez pe??a', 2, 1067943309, '0000-00-00', 3013481576, 3013481576, 'leonigutierrez@outlook.com', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(38, 'Maria Camila', 'Pareja', 2, 1015421818, '0000-00-00', 9154078378, 0, 'mariacamilapareja@hotmail.com', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(39, 'Navhys', 'Asias Martinez', 2, 1067935938, '0000-00-00', 3012393600, 3185549267, 'navhysa@gmail.com', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(40, 'Mar??a Gabriela ', 'G??mez Rodr??guez', 1, 1137974627, '0000-00-00', 3127694210, 3146609513, 'gomezfuentes444@hotmail.com', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(41, 'Valentina', 'Salgado Castilla', 2, 1003393302, '0000-00-00', 3126953741, 3126679011, 'vsalgadocastilla@hotmail.com', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(42, 'Gricelis Patricia', 'Hoyos Perez', 2, 1003644181, '0000-00-00', 3233260036, 3233260036, 'grisehoyos2599@gmail.com', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(43, 'Elienais', 'Dugarte', 4, 22762671, '0000-00-00', 3004188974, 3004188974, 'elidugarte78@gmail.com', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(44, 'Christian ', 'Rodriguez Correa', 2, 1037485093, '0000-00-00', 3104387343, 3215175092, 'crodriguez970@hotmail.com', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(45, 'DIANA KARINA', 'HERNǁNDEZ URDA', 2, 1030595998, '0000-00-00', 3006099400, 3006099400, 'dianak-17@hotmail.com', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(47, 'Juan Esteban ', 'Mu??oz figueroa', 1, 1043295249, '0000-00-00', 3006527803, 3114045729, 'jfredymc80@live.com', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(48, 'Angie Elisa', 'G??mez Pe??a', 2, 1233340124, '0000-00-00', 3226132852, 0, 'angieg_lg@hotmail.com', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(49, 'Juan Sebastiǭn ', 'Valencia Serrato', 2, 1032433927, '0000-00-00', 3123636721, 0, 'jsvalencias1989@gmail.com', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(50, 'Faustina maria', 'Barroso Herrera', 2, 1067902666, '0000-00-00', 3008340837, 3205497533, 'faustina.barroso@gmail.com', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(51, 'Nathaly', 'Saker L??pez ', 1, 1003423797, '0000-00-00', 3017400602, 3225324762, 'nathaly.saker@gmail.com', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(52, 'Alejandra ', 'Velez Pe??ate', 1, 1007822281, '0000-00-00', 3148749535, 3148749535, 'alejandravelezp@gmail.com', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(65, 'Eliana Mar??a', 'Santos Coavas', 2, 1065005969, '0000-00-00', 3226014200, 3107007091, 'eliana19sant@hotmail.com', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(66, 'FERNANDO', 'JIMENEZ PE??ATES', 2, 1047452984, '0000-00-00', 3015367207, 0, 'FERNANDOJIMENEZQF@GMAIL.COM', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(67, 'Angelica', 'Sotelo Montalvo', 1, 1003043926, '0000-00-00', 3126027267, 7865032, 'Angelicasotelo520@gmail.com', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(68, 'Maraiangel ', 'Zuleta Aleman', 1, 1062434848, '0000-00-00', 3022910602, 3114731176, 'zaracaro21@gmail.com', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(69, 'luisa fernanda ', 'fuentes argumedo', 2, 1003452749, '0000-00-00', 3158123926, 3103503822, 'luisafuentesargumedo23@gmail.com', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(70, 'Adriana ', 'Acu??a carvajal', 1, 1063355437, '0000-00-00', 3106701015, 3217757686, 'adrimaracucar@gmail.com', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(71, 'EDWIN DAVID', 'GUTIERREZ ISIDRO', 2, 1067930575, '0000-00-00', 3135618372, 3114767771, 'Edwingutierrez239@gmail.com', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(72, 'Andres', 'JimǸnez', 1, 80818313, '0000-00-00', 3017549852, 0, 'adrz1@hotmail.com', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(73, 'daicy nohemi ', 'baena escorcia ', 1, 1193078377, '0000-00-00', 3148950848, 3226363904, 'danobaes260401@gmail.com', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(74, 'Alexander', 'Qui??onez Tapia', 2, 1063280392, '0000-00-00', 3173048518, 0, 'ALEXCA2589@HOTMAIL.COM', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(75, 'tatiana andrea', 'fontalvo ramos', 2, 1067957658, '0000-00-00', 3007464064, 3046580068, 'tatiana010498@gmail.com', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(76, 'Emiro Javier', 'Casta??o Velǭsquez', 2, 1071358005, '0000-00-00', 3116548381, 3104098657, 'emirojcv1329@gmail.com', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(77, 'Victor Alberto', 'Delgado Doria', 2, 1067966126, '0000-00-00', 3155036921, 3145932694, 'victordelgado0602@hotmail.com', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(78, 'Juan Guillermo ', 'Mar??n C??rdoba', 1, 1000505816, '0000-00-00', 3053177081, 0, 'juanguillermomarinco@gmail.com', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(79, 'MARIA FERNANDA ', 'SUAREZ RAMOS', 1, 1063181742, '0000-00-00', 3045387055, 0, 'fernandasuara@gmail.com', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(80, 'HǸctor javier', 'Colina cordero', 4, 25390351, '0000-00-00', 3219473899, 0, 'hectorjavier96b@hotmail.com', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(81, 'Sara Esther', 'Fajardo Marcano', 4, 25566777, '0000-00-00', 3147771425, 3008898880, 'zarah2701@gmail.com', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(82, 'Laura Marcela', 'Betancurt Rios', 1, 1003407632, '0000-00-00', 3023121183, 0, 'lbetancurtrios@gmail.com', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(83, 'Luis Alfonso ', 'Alzǭte Madera ', 2, 1192780939, '0000-00-00', 3112380308, 3147741349, 'alfonso.91199@gmail.com', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(84, 'Yelkin', 'Arias Martinez', 2, 1049896659, '0000-00-00', 3004649118, 3052918327, 'yelkinarias@gmail.com', 'Se contact?? por ws y dijo q en el momento no tiene el presupuesto.', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(85, 'Marhiam (Acudiente: Ana Ortega Ramirez)', 'Serpa Ortega', 1, 1067946970, '0000-00-00', 3205535082, 3002029465, 'aniluna2318@gmail.com', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(86, 'Emiro JosǸ', 'Osorio Osorio', 1, 1062428495, '0000-00-00', 3015703916, 3002098016, 'emirosorio12@gmail.com', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(87, 'Juan Jose', 'Osorio Osorio', 1, 1062435536, '0000-00-00', 3015703916, 3002099967, 'emirosorio12@gmail.com', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(88, 'Maria Paulina', 'Araujo Tirado', 1, 1234526677, '0000-00-00', 3126909252, 3002666795, 'anyi0206@hotmail.com', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(89, 'etfgc', 'www', 1, 123456789, '0000-00-00', 12345678, 111, '222@hot.com', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(90, 'sindy paola ', 'monterrosa fernandez', 2, 1131072336, '0000-00-00', 3013143373, 3136157348, 'sindymonterrosa1@hotmail.com', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(91, 'Moises', 'Zabaleta', 1, 1044, '0000-00-00', 3107037820, 3107037820, 'moisesgei117@gmail.com', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(92, 'napoleon', 'zurita', 2, 1063367123, '0000-00-00', 3017096338, 0, 'napoleonzurita12@gmail.com', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(93, 'Ricky alexander', 'G??mez nieves', 2, 1042445298, '0000-00-00', 3023095894, 3012990199, 'rickyalexander21@outlook.com', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(94, 'Nicol', 'Mestra Garces', 1, 1038125504, '0000-00-00', 3004108730, 3016535356, 'rafaelmc2@colanta.com.co', 'Cancel?? $65.000', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(95, 'Rafael Ramon', 'Mestra Caldera', 2, 11002613, '0000-00-00', 3004108730, 3016535356, 'rafaelmc2@colanta.com.co', 'Pendiente $75.000', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(96, 'Yashuri', 'Paternina mercado', 2, 1067877846, '0000-00-00', 3104279624, 3012481256, 'ypaternina22@hotmail.com', 'MATRICULADA', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(97, 'Diego', 'Coronado', 2, 106874111, '1997-10-07', 3114561471, 0, 'diegocoronado@hotmail.com', '', '2018-11-28 23:34:47', '2018-11-28 23:34:47'),
(98, 'Edwin David', 'Jiménez escobar ', 1, 1003433726, '2003-09-10', 3117583030, 3145646188, 'edwinescobar057@gmail.com', '', '2018-11-30 03:04:54', '2018-11-30 03:04:54'),
(102, 'Yenifer ', 'Licona castro', 2, 1072530151, '1995-12-19', 3017751004, 3017751004, 'yliconacastro19@hotmail.com', '', '2018-12-05 03:12:18', '2018-12-05 03:12:18'),
(103, 'Danilsa', 'Ramírez García ', 1, 1003000450, '0000-00-00', 3024664395, 3128167229, 'Danilsaflower@gmail.com', '', '2018-12-05 06:56:10', '2018-12-05 06:56:10'),
(104, 'Camilo Alberto ', 'Sosa padilla', 1, 1003002540, '2002-04-09', 3135194995, 3002483950, 'sosa40758@gmail.com', '', '2018-12-08 16:22:26', '2018-12-08 16:22:26'),
(111, 'LEONARDO ENRIQUE', 'VARGAS PALOMINO', 2, 1073811086, '2000-12-03', 3004105148, 3024241295, 'leovargaspalomino@gmail.com', '', '2018-12-11 03:34:05', '2018-12-11 03:34:05'),
(112, 'Isaac Rafael', 'Cruz Sierra', 1, 1007139451, '2001-06-06', 3218289952, 3002202882, 'cruzsierra2016@hotmail.com', '', '2018-12-14 23:11:45', '2018-12-14 23:11:45'),
(115, 'Wilber David ', 'Ávila Mulasco', 1, 1003395668, '2002-01-30', 3206693233, 3145900627, 'dvcastiblanco30@gmail.com', '', '2018-12-29 00:32:27', '2018-12-29 00:32:27'),
(119, 'Andrés felipe', 'Bedoya marulanda', 1, 1, '2014-04-14', 60583082, 0, 'durleymarulanda@hotmail.com', '', '2019-01-08 21:00:08', '2019-01-08 21:00:08'),
(121, 'Andres David', 'Coronado Blanco', 2, 1067893909, '1990-09-13', 11111111, 22222222, 'andresdavidcb@gmail.com', '', '2019-01-10 16:08:10', '2019-01-10 16:08:10');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permitions`
--

CREATE TABLE `permitions` (
  `id` int(10) UNSIGNED NOT NULL,
  `role` int(11) NOT NULL,
  `module` int(11) NOT NULL,
  `privileges` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `planes`
--

CREATE TABLE `planes` (
  `id` smallint(6) NOT NULL,
  `name` varchar(40) CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `planes`
--

INSERT INTO `planes` (`id`, `name`) VALUES
(1, 'Estándar'),
(2, 'Personalizado'),
(3, 'Niños'),
(4, '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `privileges`
--

CREATE TABLE `privileges` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `id` int(10) NOT NULL,
  `name` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`id`, `name`, `created_at`, `updated_at`) VALUES
(0, 'none', NULL, NULL),
(1, 'admin', NULL, NULL),
(2, 'profesor', NULL, NULL),
(3, 'estudiante', NULL, NULL),
(4, 'asesor', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `shedules`
--

CREATE TABLE `shedules` (
  `id` smallint(6) NOT NULL,
  `day` smallint(6) NOT NULL,
  `time` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `shedules`
--

INSERT INTO `shedules` (`id`, `day`, `time`) VALUES
(1, 1, '08:00:00'),
(2, 1, '09:00:00'),
(3, 1, '10:00:00'),
(4, 1, '11:00:00'),
(5, 1, '14:00:00'),
(6, 1, '15:00:00'),
(7, 1, '16:00:00'),
(8, 1, '17:00:00'),
(9, 1, '18:00:00'),
(10, 1, '19:00:00'),
(11, 2, '08:00:00'),
(12, 2, '08:00:00'),
(13, 2, '09:00:00'),
(14, 2, '10:00:00'),
(15, 2, '11:00:00'),
(16, 2, '14:00:00'),
(17, 2, '15:00:00'),
(18, 2, '16:00:00'),
(19, 2, '17:00:00'),
(20, 2, '18:00:00'),
(21, 2, '19:00:00'),
(23, 3, '08:00:00'),
(24, 3, '09:00:00'),
(25, 3, '10:00:00'),
(26, 3, '11:00:00'),
(27, 3, '14:00:00'),
(28, 3, '15:00:00'),
(29, 3, '16:00:00'),
(30, 3, '17:00:00'),
(31, 3, '18:00:00'),
(32, 3, '19:00:00'),
(33, 4, '08:00:00'),
(34, 4, '09:00:00'),
(35, 4, '10:00:00'),
(36, 4, '11:00:00'),
(37, 4, '14:00:00'),
(38, 4, '15:00:00'),
(39, 4, '16:00:00'),
(40, 4, '17:00:00'),
(41, 4, '18:00:00'),
(42, 4, '19:00:00'),
(43, 5, '08:00:00'),
(44, 5, '09:00:00'),
(45, 5, '10:00:00'),
(46, 5, '11:00:00'),
(47, 5, '14:00:00'),
(48, 5, '15:00:00'),
(49, 5, '16:00:00'),
(50, 5, '17:00:00'),
(51, 5, '18:00:00'),
(52, 5, '19:00:00'),
(53, 6, '08:00:00'),
(54, 6, '09:00:00'),
(55, 6, '10:00:00'),
(56, 6, '11:00:00'),
(57, 6, '14:00:00'),
(58, 6, '15:00:00'),
(59, 6, '16:00:00'),
(60, 6, '17:00:00'),
(61, 6, '18:00:00'),
(62, 6, '19:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `students`
--
-- Error leyendo la estructura de la tabla oasisac1_academia.students: #1932 - Table 'oasisac1_academia.students' doesn't exist in engine
-- Error leyendo datos de la tabla oasisac1_academia.students: #1064 - Algo está equivocado en su sintax cerca 'FROM `oasisac1_academia`.`students`' en la linea 1

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `teachers`
--

CREATE TABLE `teachers` (
  `id` int(10) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lastname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `id_type` smallint(6) NOT NULL,
  `identification` bigint(20) NOT NULL,
  `birthdate` date NOT NULL,
  `main_phone` bigint(20) NOT NULL,
  `sec_phone` bigint(20) NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `observations` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `teachers`
--

INSERT INTO `teachers` (`id`, `name`, `lastname`, `id_type`, `identification`, `birthdate`, `main_phone`, `sec_phone`, `email`, `address`, `observations`, `created_at`, `updated_at`) VALUES
(1, 'Gabriel', 'Coronado Blanco', 2, 1067913096, '1991-05-13', 3207473072, 7893190, 'gabrielcb007@gmail.com', 'Carrera 14c No. 48-42', '', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `role` smallint(6) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `role`, `remember_token`, `created_at`, `updated_at`) VALUES
(0, 'autoregister', '', '', 0, NULL, NULL, NULL),
(1, 'Gabriel Coronado', 'gabrielcb007@gmail.com', '$2y$10$JSpQyb/FNfC41qtYDr24aO2/62kqALV1ZPc4ijNR/DuUxZFfEooOi', 1, 'R0dHnlT7DTOHk03QDOobAOTfLS5iila1rZM9FKU1voJLmI71DtPIzQ8yq8N6', '2017-05-24 21:54:20', '2018-12-28 20:32:54');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_statuses`
--

CREATE TABLE `user_statuses` (
  `id` smallint(6) NOT NULL,
  `name` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `user_statuses`
--

INSERT INTO `user_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'activo', NULL, NULL),
(2, 'anulado', NULL, NULL);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `course_category` (`course_category`),
  ADD KEY `plan` (`plan`),
  ADD KEY `nivel` (`level`),
  ADD KEY `shedule` (`shedule`),
  ADD KEY `teacher` (`teacher`);

--
-- Indices de la tabla `course_categories`
--
ALTER TABLE `course_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `days`
--
ALTER TABLE `days`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `enrolments`
--
ALTER TABLE `enrolments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `payment_status` (`payment_status`),
  ADD KEY `student` (`student`),
  ADD KEY `course` (`course`);

--
-- Indices de la tabla `enrolment_statuses`
--
ALTER TABLE `enrolment_statuses`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `forms`
--
ALTER TABLE `forms`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `id_types`
--
ALTER TABLE `id_types`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `levels`
--
ALTER TABLE `levels`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `modules`
--
ALTER TABLE `modules`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indices de la tabla `people`
--
ALTER TABLE `people`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `identification` (`identification`);

--
-- Indices de la tabla `permitions`
--
ALTER TABLE `permitions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permitions_role_index` (`role`),
  ADD KEY `permitions_module_index` (`module`),
  ADD KEY `permitions_privileges_index` (`privileges`);

--
-- Indices de la tabla `planes`
--
ALTER TABLE `planes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `privileges`
--
ALTER TABLE `privileges`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `shedules`
--
ALTER TABLE `shedules`
  ADD PRIMARY KEY (`id`),
  ADD KEY `day` (`day`);

--
-- Indices de la tabla `teachers`
--
ALTER TABLE `teachers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `identification` (`identification`),
  ADD KEY `teachers_id_type_index` (`id_type`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `role` (`role`);

--
-- Indices de la tabla `user_statuses`
--
ALTER TABLE `user_statuses`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `courses`
--
ALTER TABLE `courses`
  MODIFY `id` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `course_categories`
--
ALTER TABLE `course_categories`
  MODIFY `id` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `days`
--
ALTER TABLE `days`
  MODIFY `id` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `enrolments`
--
ALTER TABLE `enrolments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `enrolment_statuses`
--
ALTER TABLE `enrolment_statuses`
  MODIFY `id` tinyint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `forms`
--
ALTER TABLE `forms`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `id_types`
--
ALTER TABLE `id_types`
  MODIFY `id` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `levels`
--
ALTER TABLE `levels`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `modules`
--
ALTER TABLE `modules`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `people`
--
ALTER TABLE `people`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=122;

--
-- AUTO_INCREMENT de la tabla `permitions`
--
ALTER TABLE `permitions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `planes`
--
ALTER TABLE `planes`
  MODIFY `id` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `privileges`
--
ALTER TABLE `privileges`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `shedules`
--
ALTER TABLE `shedules`
  MODIFY `id` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;

--
-- AUTO_INCREMENT de la tabla `teachers`
--
ALTER TABLE `teachers`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `user_statuses`
--
ALTER TABLE `user_statuses`
  MODIFY `id` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `courses`
--
ALTER TABLE `courses`
  ADD CONSTRAINT `courses_ibfk_1` FOREIGN KEY (`course_category`) REFERENCES `course_categories` (`id`);

--
-- Filtros para la tabla `shedules`
--
ALTER TABLE `shedules`
  ADD CONSTRAINT `shedules_ibfk_1` FOREIGN KEY (`day`) REFERENCES `days` (`id`);

--
-- Filtros para la tabla `teachers`
--
ALTER TABLE `teachers`
  ADD CONSTRAINT `teachers_ibfk_1` FOREIGN KEY (`id_type`) REFERENCES `id_types` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
