﻿-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 01-08-2018 a las 12:13:30
-- Versión del servidor: 10.2.16-MariaDB
-- Versión de PHP: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!401

INSERT INTO `people` (`id`, `name`, `lastname`, `id_type`, `identification`, `birthdate`, `main_phone`, `sec_phone`, `interest_courses`, `email`, `observations`, `enrolment_status`, `created_at`, `updated_at`) VALUES
(2, 'Luis felipe', 'Sáez aparicio', 2, 1067916515, '1992-10-12', 3207944050, 3145730610, 'guitarra,canto', 'pipesaez2@hotmail.com', '', 1, '2018-01-10 19:05:08', '2018-01-10 19:05:08'),
(3, 'Victor Daniel', 'Negrete Baza', 1, 1067865824, '2003-12-19', 3045889142, 3128934257, 'guitarra', 'negretebazavictordaniel@gmail.com', '', 1, '2018-01-17 00:52:13', '2018-01-17 00:52:13'),
(4, 'lorena', 'vergara', 2, 1102809038, '1987-03-09', 3042132480, 0, 'canto', 'loremaver@gmail.com', '', 1, '2018-01-25 08:13:28', '2018-01-25 08:13:28'),
(5, 'Isabel', 'Bulasco ', 1, 103577865, '2006-02-07', 3017787102, 7860657, 'guitarra,canto', 'Isabel020706@hotmail.com', '', 1, '2018-01-25 21:30:30', '2018-01-25 21:30:30'),
(6, 'pedro luis', 'trujillo rios ', 2, 1233343079, '1998-09-28', 3116105143, 347953927, 'guitarra,canto', 'wplay123@outlook.com', '', 1, '2018-01-29 23:22:08', '2018-01-29 23:22:08'),
(8, 'Jhan Carlos', 'Viloria Murillo', 2, 1007781249, '0000-00-00', 3215012831, 3215649925, 'batería', 'jhanviloria45@gmail.com', '', 1, '2018-02-05 21:52:38', '2018-02-05 21:52:38'),
(9, 'Jonathan', 'Dix Polo', 1, 1002998196, '0000-00-00', 7866062, 0, 'piano,canto', 'jonathandix12@gmail.com', '', 1, '2018-02-08 18:09:56', '2018-02-08 18:09:56'),
(11, 'juan sebastian ', 'ballen manosalva', 2, 1116807689, '1997-12-03', 3184677781, 3165462227, 'guitarra', 'sebastianballenm@hotmail.com', '', 1, '2018-02-17 02:30:51', '2018-02-17 02:30:51'),
(12, 'Daniel de jesus', 'Holguin Ricardo', 2, 1063294830, '1993-09-10', 3122493144, 3103753884, 'guitarra,canto', 'daniel.hr1@hotmail.com', '', 1, '2018-02-20 03:57:02', '2018-02-20 03:57:02'),
(13, 'Marcela', 'Martinez Babilonia', 2, 1007339981, '1995-08-19', 3126010564, 0, 'guitarra,canto', 'babiloniatheblackrose@gmail.com', '', 1, '2018-02-20 21:37:12', '2018-02-20 21:37:12'),
(14, 'Luciano ', 'Ferreras', 1, 37369943, '1994-04-10', 542215027524, 0, 'batería', 'lucianoferrerasmdh@gmail.com', '', 1, '2018-02-22 11:55:38', '2018-02-22 11:55:38'),
(15, 'Isaid joseth', 'Martinez martinez', 2, 1102840883, '1992-07-16', 3216515130, 0, 'guitarra,piano,canto', 'isaidmarti7@hotmail.com', '', 1, '2018-02-26 02:01:04', '2018-02-26 02:01:04'),
(16, 'Magnoria', 'Gomez Plata', 2, 37948782, '1982-10-08', 3145639249, 0, 'piano', 'margy-08@hotmail.com', '', 1, '2018-03-01 21:32:52', '2018-03-01 21:32:52'),
(18, 'Moisés David', 'Guzmán Pretel', 2, 1067938918, '1995-02-26', 3013663186, 3013663186, 'guitarra,canto', 'moisesdavid0010@gmail.com', '', 1, '2018-03-07 07:26:28', '2018-03-07 07:26:28'),
(19, 'Yury Estela', 'Sanchez Calderon', 2, 1067916327, '1992-10-22', 3005985881, 3008865408, 'piano', 'tete.sanchez22@hotmail.com', '', 1, '2018-03-12 18:02:41', '2018-03-12 18:02:41'),
(20, 'Julio Antonio', 'Peniche Maestre', 2, 1069490233, '1992-09-06', 3043394463, 3135537781, 'canto', 'julioantonio_06@hotmail.com', '', 1, '2018-03-15 00:05:38', '2018-03-15 00:05:38'),
(21, 'Juan José', 'Ely Romero', 2, 1068587999, '1995-06-10', 3135537781, 3043394463, 'canto', 'jjelima@gmail.com', '', 1, '2018-03-15 00:09:02', '2018-03-15 00:09:02'),
(22, 'CLAUDIA MARIA ', 'DICKSON ROMERO', 2, 1067867929, '1988-07-06', 3218925019, 7894408, 'canto', 'claudickr@hotmail.com', '', 1, '2018-03-15 02:09:30', '2018-03-15 02:09:30'),
(23, 'CESAR', 'GONZALEZ', 2, 1002161904, '1999-07-30', 3003419824, 0, 'guitarra', 'cesar30992010@gmail.com', '', 1, '2018-03-22 01:25:30', '2018-03-22 01:25:30'),
(24, 'Mariluz', 'Alavarez Correa', 2, 1063146540, '1988-03-21', 3205376551, 3127909435, 'piano', 'mariluzalvarezcorrea@hotmail.com', '', 1, '2018-03-27 18:19:29', '2018-03-27 18:19:29'),
(25, 'Leandra', 'Medina', 1, 1003433895, '2000-09-07', 3106392296, 0, 'piano', 'leandramedina0707@gmail.com', '', 1, '2018-03-30 17:44:18', '2018-03-30 17:44:18'),
(26, 'Mónica', 'Ramírez Ramírez', 2, 1041234851, '1999-03-05', 3147378428, 3147378428, 'saxofón', 'monimar1999@hotmail.com', '', 1, '2018-04-03 22:40:04', '2018-04-03 22:40:04'),
(27, 'Roberto Jose', 'Saenz Avilez', 2, 11156367, '1985-09-06', 3135859575, 0, 'piano', 'ingrobertosaenz@gmail.com', '', 1, '2018-04-03 23:06:14', '2018-04-03 23:06:14'),
(28, 'Victor Hugo', 'Gomez Hernanez', 2, 16070977, '0000-00-00', 3128432909, 0, 'guitarra,canto', 'victorgomezmanizales@yahoo.com', '', 1, '2018-04-05 19:16:06', '2018-04-05 19:16:06'),
(29, 'Kevin alexander', 'londoño asprilla', 2, 1020456429, '1993-11-23', 3045374799, 3045374799, 'guitarra', 'kevin6londono@hotmail.com', '', 1, '2018-04-06 00:54:19', '2018-04-06 00:54:19'),
(30, 'Jesús Daniel', 'Narváez Mercado', 1, 1003069818, '2002-10-26', 3013275943, 3023025593, 'guitarra,canto', 'dajename26@hotmail.com', '', 1, '2018-04-07 20:22:12', '2018-04-07 20:22:12'),
(31, 'Santiago ', 'Nuñez ', 1, 1003432142, '2001-10-26', 3107417987, 0, 'canto', 'santiagonunez26@icloud.com', '', 1, '2018-04-19 19:54:59', '2018-04-19 19:54:59'),
(32, 'LUIS FERNANDO', 'CABALLERO MORALES', 2, 1068819539, '1994-09-23', 3114000749, 0, 'piano', 'luis_caballero23@hotmail.es', '', 1, '2018-04-29 19:25:15', '2018-04-29 19:25:15'),
(33, 'Julio cesar', 'Argumedo hoyos', 2, 1067958072, '1998-10-26', 3215360021, 0, 'piano', 'argumedojuliocesar53@gmail.com', '', 1, '2018-05-08 16:58:22', '2018-05-08 16:58:22'),
(34, 'Vanessa', 'blanco', 1, 1000412991, '2002-08-09', 3103646569, 0, 'piano,canto', 'Vanessacalderon34blanco@gmail.com', '', 1, '2018-05-14 19:35:46', '2018-05-14 19:35:46'),
(35, 'Luis Fernando', 'Giraldo Diaz', 2, 1002998887, '2000-02-15', 3015800591, 3126109060, 'piano', 'yiraldfeer2014@gmail.com', '', 1, '2018-05-17 18:48:27', '2018-05-17 18:48:27'),
(36, 'Juan Pablo', 'Vidal Atencio', 2, 1067958988, '1998-03-07', 3216824537, 3135092269, 'guitarra', 'juanpa_0703@hotmail.com', '', 1, '2018-05-25 00:22:51', '2018-05-25 00:22:51'),
(37, 'Lina Marcela', 'Gutiérrez peña', 2, 1067943309, '1995-08-05', 3013481576, 3013481576, 'canto', 'leonigutierrez@outlook.com', '', 1, '2018-05-25 18:55:10', '2018-05-25 18:55:10'),
(38, 'Maria Camila', 'Pareja', 2, 1015421818, '1990-12-08', 9154078378, 0, 'piano', 'mariacamilapareja@hotmail.com', '', 1, '2018-05-29 21:56:50', '2018-05-29 21:56:50'),
(39, 'Navhys', 'Asias Martinez', 2, 1067935938, '0000-00-00', 3012393600, 3185549267, 'piano', 'navhysa@gmail.com', '', 1, '2018-06-06 04:47:15', '2018-06-06 04:47:15'),
(40, 'María Gabriela ', 'Gómez Rodríguez', 1, 1137974627, '2005-02-14', 3127694210, 3146609513, 'guitarra', 'gomezfuentes444@hotmail.com', '', 1, '2018-06-14 21:34:42', '2018-06-14 21:34:42'),
(41, 'Valentina', 'Salgado Castilla', 2, 1003393302, '2000-01-04', 3126953741, 3126679011, 'piano', 'vsalgadocastilla@hotmail.com', '', 1, '2018-06-15 22:43:42', '2018-06-15 22:43:42'),
(42, 'Gricelis Patricia', 'Hoyos Perez', 2, 1003644181, '1999-10-25', 3233260036, 3233260036, 'canto', 'grisehoyos2599@gmail.com', '', 1, '2018-06-21 03:43:37', '2018-06-21 03:43:37'),
(43, 'Elienais', 'Dugarte', 4, 22762671, '2018-06-28', 3004188974, 3004188974, 'canto', 'elidugarte78@gmail.com', '', 1, '2018-06-21 07:26:51', '2018-06-21 07:26:51'),
(44, 'Christian ', 'Rodriguez Correa', 2, 1037485093, '1997-09-22', 3104387343, 3215175092, 'guitarra', 'crodriguez970@hotmail.com', '', 1, '2018-06-22 22:37:35', '2018-06-22 22:37:35'),
(45, 'DIANA KARINA', 'HERNÁNDEZ URDA', 2, 1030595998, '1991-06-17', 3006099400, 3006099400, 'guitarra,piano,batería,canto', 'dianak-17@hotmail.com', '', 1, '2018-06-24 18:30:00', '2018-06-24 18:30:00'),
(46, 'wendy ', 'suarez', 1, 1, '2000-11-11', 3186673829, 3043988972, 'piano,canto', 'wendycabrales@hotmail.com', '', 1, '2018-06-26 01:01:38', '2018-06-26 01:01:38'),
(47, 'Juan Esteban ', 'Muñoz figueroa', 1, 1043295249, '2004-09-30', 3006527803, 3114045729, 'canto', 'jfredymc80@live.com', '', 1, '2018-06-27 20:05:57', '2018-06-27 20:05:57'),
(48, 'Angie Elisa', 'Gómez Peña', 2, 1233340124, '1997-09-09', 3226132852, 0, 'guitarra', 'angieg_lg@hotmail.com', '', 1, '2018-07-05 17:41:30', '2018-07-05 17:41:30'),
(49, 'Juan Sebastián ', 'Valencia Serrato', 2, 1032433927, '1989-11-13', 3123636721, 0, 'batería', 'jsvalencias1989@gmail.com', '', 1, '2018-07-06 20:55:49', '2018-07-06 20:55:49'),
(50, 'Faustina maria', 'Barroso Herrera', 2, 1067902666, '1991-05-03', 3008340837, 3205497533, 'piano', 'faustina.barroso@gmail.com', '', 1, '2018-07-07 05:16:00', '2018-07-07 05:16:00'),
(51, 'Nathaly', 'Saker López ', 1, 1003423797, '2002-09-17', 3017400602, 3225324762, 'canto', 'nathaly.saker@gmail.com', '', 1, '2018-07-09 22:37:00', '2018-07-09 22:37:00'),
(52, 'Alejandra ', 'Velez Peñate', 1, 1007822281, '2002-03-18', 3148749535, 3148749535, 'canto', 'alejandravelezp@gmail.com', '', 1, '2018-07-11 00:28:56', '2018-07-11 00:28:56'),
(65, 'Eliana María', 'Santos Coavas', 2, 1065005969, '1994-08-06', 3226014200, 3107007091, 'guitarra,piano,canto', 'eliana19sant@hotmail.com', '', 1, '2018-07-22 04:38:51', '2018-07-22 04:38:51'),
(66, 'FERNANDO', 'JIMENEZ PEÑATES', 2, 1047452984, '1993-01-29', 3015367207, 0, 'canto', 'FERNANDOJIMENEZQF@GMAIL.COM', '', 1, '2018-07-26 00:31:51', '2018-07-26 00:31:51');

