-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 21-11-2018 a las 23:05:49
-- Versión del servidor: 5.6.20
-- Versión de PHP: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `oasis`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `courses`
--

CREATE TABLE IF NOT EXISTS `courses` (
`id` smallint(6) NOT NULL,
  `name` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `course_category` smallint(6) NOT NULL,
  `standar_price` decimal(10,0) NOT NULL,
  `level` smallint(6) NOT NULL,
  `plan` smallint(6) NOT NULL,
  `shedule` smallint(6) NOT NULL,
  `teacher` int(10) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `course_categories`
--

CREATE TABLE IF NOT EXISTS `course_categories` (
`id` smallint(6) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=10 ;

--
-- Volcado de datos para la tabla `course_categories`
--

INSERT INTO `course_categories` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Piano', NULL, NULL),
(2, 'Guitarra', NULL, NULL),
(3, 'Saxofón', NULL, NULL),
(4, 'Vocalización', NULL, NULL),
(5, 'Bajo', NULL, NULL),
(6, 'Batería', NULL, NULL),
(7, 'Acordeón', NULL, NULL),
(8, 'Chelo', NULL, NULL),
(9, 'Violín', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `days`
--

CREATE TABLE IF NOT EXISTS `days` (
`id` smallint(6) NOT NULL,
  `day` varchar(15) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Volcado de datos para la tabla `days`
--

INSERT INTO `days` (`id`, `day`) VALUES
(1, 'Lunes'),
(2, 'Martes'),
(3, 'Miércoles'),
(4, 'Jueves'),
(5, 'Viernes'),
(6, 'Sábado');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `enrolments`
--

CREATE TABLE IF NOT EXISTS `enrolments` (
`id` int(10) unsigned NOT NULL,
  `student` int(11) NOT NULL,
  `course` smallint(6) NOT NULL,
  `payment_status` smallint(6) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `enrolment_statuses`
--

CREATE TABLE IF NOT EXISTS `enrolment_statuses` (
`id` smallint(6) NOT NULL,
  `name` varchar(20) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `enrolment_statuses`
--

INSERT INTO `enrolment_statuses` (`id`, `name`) VALUES
(1, 'preinscrito'),
(2, 'matriculado'),
(3, 'inactivo'),
(4, 'retirado');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `forms`
--

CREATE TABLE IF NOT EXISTS `forms` (
`id` int(10) NOT NULL,
  `module` int(11) NOT NULL,
  `route` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `model` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `table_label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fields` text COLLATE utf8_unicode_ci NOT NULL,
  `create_fields` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `id_types`
--

CREATE TABLE IF NOT EXISTS `id_types` (
`id` smallint(6) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Volcado de datos para la tabla `id_types`
--

INSERT INTO `id_types` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Tarjeta de identidad', NULL, NULL),
(2, 'Cédula de ciudadanía', NULL, NULL),
(3, 'Pasaporte', NULL, NULL),
(4, 'Cédula de extranjería', NULL, NULL),
(5, 'Registro civil', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `interest_courses`
--

CREATE TABLE IF NOT EXISTS `interest_courses` (
  `id` int(11) NOT NULL,
  `course` smallint(6) NOT NULL,
  `student` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `levels`
--

CREATE TABLE IF NOT EXISTS `levels` (
`id` smallint(6) NOT NULL,
  `name` varchar(20) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `levels`
--

INSERT INTO `levels` (`id`, `name`) VALUES
(1, 'Básico'),
(2, 'Intermedio'),
(3, 'Avanzado');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `modules`
--

CREATE TABLE IF NOT EXISTS `modules` (
`id` int(10) NOT NULL,
  `title` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `modules`
--

INSERT INTO `modules` (`id`, `title`, `name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'administración', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `payment_satuses`
--

CREATE TABLE IF NOT EXISTS `payment_satuses` (
`id` smallint(6) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `payment_satuses`
--

INSERT INTO `payment_satuses` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Pendiente', NULL, NULL),
(2, 'Pagado', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `people`
--

CREATE TABLE IF NOT EXISTS `people` (
`id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `lastname` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `id_type` smallint(6) NOT NULL,
  `identification` bigint(20) NOT NULL,
  `main_phone` bigint(20) NOT NULL,
  `sec_phone` bigint(20) NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=15 ;

--
-- Volcado de datos para la tabla `people`
--

INSERT INTO `people` (`id`, `name`, `lastname`, `id_type`, `identification`, `main_phone`, `sec_phone`, `email`, `created_at`, `updated_at`) VALUES
(1, 'Andres', 'Coronado', 2, 1067893909, 3218821181, 3207473072, 'andresdavidcb@gmail.com', '2018-11-20 17:40:16', '2018-11-20 17:40:16'),
(14, 'Gabriel', 'Coronado Blanco', 2, 1067913096, 3207473072, 111111111, 'gabrielcb_007@gmail.com', '2018-11-22 02:42:39', '2018-11-22 02:42:39');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permitions`
--

CREATE TABLE IF NOT EXISTS `permitions` (
`id` int(10) unsigned NOT NULL,
  `role` int(11) NOT NULL,
  `module` int(11) NOT NULL,
  `privileges` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `planes`
--

CREATE TABLE IF NOT EXISTS `planes` (
`id` smallint(6) NOT NULL,
  `name` varchar(40) CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `planes`
--

INSERT INTO `planes` (`id`, `name`) VALUES
(1, 'Estándar'),
(2, 'Personalizado'),
(3, 'Niños'),
(4, '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `privileges`
--

CREATE TABLE IF NOT EXISTS `privileges` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
`id` smallint(6) NOT NULL,
  `name` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'admin', NULL, NULL),
(2, 'profesor', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `shedules`
--

CREATE TABLE IF NOT EXISTS `shedules` (
`id` smallint(6) NOT NULL,
  `day` smallint(6) NOT NULL,
  `time` time NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=63 ;

--
-- Volcado de datos para la tabla `shedules`
--

INSERT INTO `shedules` (`id`, `day`, `time`) VALUES
(1, 1, '08:00:00'),
(2, 1, '09:00:00'),
(3, 1, '10:00:00'),
(4, 1, '11:00:00'),
(5, 1, '14:00:00'),
(6, 1, '15:00:00'),
(7, 1, '16:00:00'),
(8, 1, '17:00:00'),
(9, 1, '18:00:00'),
(10, 1, '19:00:00'),
(11, 2, '08:00:00'),
(12, 2, '08:00:00'),
(13, 2, '09:00:00'),
(14, 2, '10:00:00'),
(15, 2, '11:00:00'),
(16, 2, '14:00:00'),
(17, 2, '15:00:00'),
(18, 2, '16:00:00'),
(19, 2, '17:00:00'),
(20, 2, '18:00:00'),
(21, 2, '19:00:00'),
(23, 3, '08:00:00'),
(24, 3, '09:00:00'),
(25, 3, '10:00:00'),
(26, 3, '11:00:00'),
(27, 3, '14:00:00'),
(28, 3, '15:00:00'),
(29, 3, '16:00:00'),
(30, 3, '17:00:00'),
(31, 3, '18:00:00'),
(32, 3, '19:00:00'),
(33, 4, '08:00:00'),
(34, 4, '09:00:00'),
(35, 4, '10:00:00'),
(36, 4, '11:00:00'),
(37, 4, '14:00:00'),
(38, 4, '15:00:00'),
(39, 4, '16:00:00'),
(40, 4, '17:00:00'),
(41, 4, '18:00:00'),
(42, 4, '19:00:00'),
(43, 5, '08:00:00'),
(44, 5, '09:00:00'),
(45, 5, '10:00:00'),
(46, 5, '11:00:00'),
(47, 5, '14:00:00'),
(48, 5, '15:00:00'),
(49, 5, '16:00:00'),
(50, 5, '17:00:00'),
(51, 5, '18:00:00'),
(52, 5, '19:00:00'),
(53, 6, '08:00:00'),
(54, 6, '09:00:00'),
(55, 6, '10:00:00'),
(56, 6, '11:00:00'),
(57, 6, '14:00:00'),
(58, 6, '15:00:00'),
(59, 6, '16:00:00'),
(60, 6, '17:00:00'),
(61, 6, '18:00:00'),
(62, 6, '19:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `students`
--

CREATE TABLE IF NOT EXISTS `students` (
`id` int(10) unsigned NOT NULL,
  `person` bigint(20) NOT NULL,
  `interest_courses` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `enrolment_status` smallint(6) NOT NULL DEFAULT '1',
  `observations` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` smallint(6) NOT NULL DEFAULT '0'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `students`
--

INSERT INTO `students` (`id`, `person`, `interest_courses`, `enrolment_status`, `observations`, `created_at`, `updated_at`, `created_by`) VALUES
(1, 1, 'guitarra', 1, '', NULL, NULL, 2),
(2, 14, '', 1, '', '2018-11-22 02:42:39', '2018-11-22 02:42:39', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `teachers`
--

CREATE TABLE IF NOT EXISTS `teachers` (
`id` int(10) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lastname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `id_type` smallint(6) NOT NULL,
  `identification` bigint(20) NOT NULL,
  `birthdate` date NOT NULL,
  `main_phone` bigint(20) NOT NULL,
  `sec_phone` bigint(20) NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `observations` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `teachers`
--

INSERT INTO `teachers` (`id`, `name`, `lastname`, `id_type`, `identification`, `birthdate`, `main_phone`, `sec_phone`, `email`, `address`, `observations`, `created_at`, `updated_at`) VALUES
(1, 'Gabriel', 'Coronado', 2, 1067913096, '1991-05-13', 3207473072, 78963211, 'gabrielcb007@gmail.com', 'Monteverde', 'ggggg', NULL, '2018-10-10 04:21:37'),
(4, 'Luis', 'García ', 2, 1321321, '1996-05-05', 513216512, 45612131, 'asdfasl@gjsdjk.com', 'sdfak dasmklnas', 'sdav', '2018-07-26 02:07:23', '2018-07-26 02:07:23');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`id` int(10) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `role` smallint(6) NOT NULL DEFAULT '1',
  `status` smallint(6) NOT NULL DEFAULT '1',
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `role`, `status`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(0, 'autoregister', 'reply@oasisacademia.com', 1, 1, '$2y$10$UREW5lv31dl8oh10ZjoOt.RAmgK79u79wpZc4LTGZxWZIO5Y2BXhe', 'UsnHjaaUzjMcg3Dy2eNbxfuunRE9e7OebXdIp0v9sKcCFiiihGee5CtXUKwN', '2018-08-02 05:41:08', '2018-08-02 05:41:56'),
(2, 'Gabriel Coronado', 'gabrielcb_007@gmail.com', 1, 1, '$2y$10$xjUomYJk9XlDrLHOA31lcuUDnUOHGEeRhJDv155Fjq.EpPux5Gp/m', '2SZ3VLru1nzq5rHEFOQ9yJ70TigPYL4u4gPwABPLxvnkpHSoNVMDwuDsPwNf', '2017-07-01 20:14:34', '2018-02-27 03:14:27'),
(3, 'Andrés David C. B.', 'andresdavidcb@gmail.com', 1, 1, '$2y$10$JL/PKLkpTHtHVtm8ArbOjuXpBlsTftfPYTiEh9JShhPUtJ1ecoxL6', 'DRfwPqL2fv36eUtFOC9PC3nVpbF9LEzwC2J73bO7WCNYqFSWJlk3rkevh8g7', '2017-07-01 20:17:00', '2018-11-20 02:27:04');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_statuses`
--

CREATE TABLE IF NOT EXISTS `user_statuses` (
`id` smallint(6) NOT NULL,
  `name` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `user_statuses`
--

INSERT INTO `user_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'activo', NULL, NULL),
(2, 'anulado', NULL, NULL);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `courses`
--
ALTER TABLE `courses`
 ADD PRIMARY KEY (`id`), ADD KEY `course_category` (`course_category`), ADD KEY `level` (`level`), ADD KEY `plan` (`plan`), ADD KEY `shedule` (`shedule`), ADD KEY `teacher` (`teacher`);

--
-- Indices de la tabla `course_categories`
--
ALTER TABLE `course_categories`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `days`
--
ALTER TABLE `days`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `enrolments`
--
ALTER TABLE `enrolments`
 ADD PRIMARY KEY (`id`), ADD KEY `payment_status` (`payment_status`), ADD KEY `student` (`student`), ADD KEY `course` (`course`);

--
-- Indices de la tabla `enrolment_statuses`
--
ALTER TABLE `enrolment_statuses`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `forms`
--
ALTER TABLE `forms`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `id_types`
--
ALTER TABLE `id_types`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `interest_courses`
--
ALTER TABLE `interest_courses`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `levels`
--
ALTER TABLE `levels`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `modules`
--
ALTER TABLE `modules`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
 ADD KEY `password_resets_email_index` (`email`), ADD KEY `password_resets_token_index` (`token`);

--
-- Indices de la tabla `payment_satuses`
--
ALTER TABLE `payment_satuses`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `people`
--
ALTER TABLE `people`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `identification` (`identification`), ADD KEY `id_type` (`id_type`);

--
-- Indices de la tabla `permitions`
--
ALTER TABLE `permitions`
 ADD PRIMARY KEY (`id`), ADD KEY `permitions_role_index` (`role`), ADD KEY `permitions_module_index` (`module`), ADD KEY `permitions_privileges_index` (`privileges`);

--
-- Indices de la tabla `planes`
--
ALTER TABLE `planes`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `privileges`
--
ALTER TABLE `privileges`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `shedules`
--
ALTER TABLE `shedules`
 ADD PRIMARY KEY (`id`), ADD KEY `day` (`day`);

--
-- Indices de la tabla `students`
--
ALTER TABLE `students`
 ADD PRIMARY KEY (`id`), ADD KEY `person` (`person`), ADD KEY `person_2` (`person`);

--
-- Indices de la tabla `teachers`
--
ALTER TABLE `teachers`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `identification` (`identification`), ADD KEY `teachers_id_type_index` (`id_type`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `users_email_unique` (`email`), ADD KEY `role` (`role`), ADD KEY `status` (`status`);

--
-- Indices de la tabla `user_statuses`
--
ALTER TABLE `user_statuses`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `courses`
--
ALTER TABLE `courses`
MODIFY `id` smallint(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `course_categories`
--
ALTER TABLE `course_categories`
MODIFY `id` smallint(6) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT de la tabla `days`
--
ALTER TABLE `days`
MODIFY `id` smallint(6) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `enrolments`
--
ALTER TABLE `enrolments`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `enrolment_statuses`
--
ALTER TABLE `enrolment_statuses`
MODIFY `id` smallint(6) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `forms`
--
ALTER TABLE `forms`
MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `id_types`
--
ALTER TABLE `id_types`
MODIFY `id` smallint(6) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `levels`
--
ALTER TABLE `levels`
MODIFY `id` smallint(6) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `modules`
--
ALTER TABLE `modules`
MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `payment_satuses`
--
ALTER TABLE `payment_satuses`
MODIFY `id` smallint(6) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `people`
--
ALTER TABLE `people`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT de la tabla `permitions`
--
ALTER TABLE `permitions`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `planes`
--
ALTER TABLE `planes`
MODIFY `id` smallint(6) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `privileges`
--
ALTER TABLE `privileges`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `roles`
--
ALTER TABLE `roles`
MODIFY `id` smallint(6) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `shedules`
--
ALTER TABLE `shedules`
MODIFY `id` smallint(6) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=63;
--
-- AUTO_INCREMENT de la tabla `students`
--
ALTER TABLE `students`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `teachers`
--
ALTER TABLE `teachers`
MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `user_statuses`
--
ALTER TABLE `user_statuses`
MODIFY `id` smallint(6) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `courses`
--
ALTER TABLE `courses`
ADD CONSTRAINT `courses_ibfk_1` FOREIGN KEY (`course_category`) REFERENCES `course_categories` (`id`),
ADD CONSTRAINT `courses_ibfk_2` FOREIGN KEY (`level`) REFERENCES `levels` (`id`),
ADD CONSTRAINT `courses_ibfk_3` FOREIGN KEY (`plan`) REFERENCES `planes` (`id`),
ADD CONSTRAINT `courses_ibfk_4` FOREIGN KEY (`shedule`) REFERENCES `shedules` (`id`),
ADD CONSTRAINT `courses_ibfk_5` FOREIGN KEY (`teacher`) REFERENCES `teachers` (`id`);

--
-- Filtros para la tabla `enrolments`
--
ALTER TABLE `enrolments`
ADD CONSTRAINT `enrolments_ibfk_1` FOREIGN KEY (`payment_status`) REFERENCES `payment_satuses` (`id`),
ADD CONSTRAINT `enrolments_ibfk_3` FOREIGN KEY (`course`) REFERENCES `courses` (`id`);

--
-- Filtros para la tabla `shedules`
--
ALTER TABLE `shedules`
ADD CONSTRAINT `shedules_ibfk_1` FOREIGN KEY (`day`) REFERENCES `days` (`id`);

--
-- Filtros para la tabla `teachers`
--
ALTER TABLE `teachers`
ADD CONSTRAINT `teachers_ibfk_1` FOREIGN KEY (`id_type`) REFERENCES `id_types` (`id`);

--
-- Filtros para la tabla `users`
--
ALTER TABLE `users`
ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`role`) REFERENCES `roles` (`id`),
ADD CONSTRAINT `users_ibfk_2` FOREIGN KEY (`status`) REFERENCES `user_statuses` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
