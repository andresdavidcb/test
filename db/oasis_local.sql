-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 06-02-2019 a las 07:22:51
-- Versión del servidor: 10.1.37-MariaDB
-- Versión de PHP: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `oasis_test`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `courses`
--

CREATE TABLE `courses` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `course_category` smallint(5) UNSIGNED NOT NULL,
  `level` smallint(5) UNSIGNED NOT NULL,
  `plan` smallint(5) UNSIGNED NOT NULL,
  `price` decimal(12,2) NOT NULL,
  `shedule` smallint(5) UNSIGNED NOT NULL,
  `teacher` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `course_categories`
--

CREATE TABLE `course_categories` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `course_categories`
--

INSERT INTO `course_categories` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Piano', NULL, NULL),
(2, 'Guitarra', NULL, NULL),
(3, 'Saxofón', NULL, NULL),
(4, 'Vocalización', NULL, NULL),
(5, 'Bajo', NULL, NULL),
(6, 'Batería', NULL, NULL),
(7, 'Acordeón', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `days`
--

CREATE TABLE `days` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `days`
--

INSERT INTO `days` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Lunes', NULL, NULL),
(2, 'Martes', NULL, NULL),
(3, 'Miércoles', NULL, NULL),
(4, 'Jueves', NULL, NULL),
(5, 'Viernes', NULL, NULL),
(6, 'Sábado', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `enrolments`
--

CREATE TABLE `enrolments` (
  `id` int(10) UNSIGNED NOT NULL,
  `student` int(11) NOT NULL,
  `course` int(11) NOT NULL,
  `payment_status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `enrolment_statuses`
--

CREATE TABLE `enrolment_statuses` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `enrolment_statuses`
--

INSERT INTO `enrolment_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Preinscrito', NULL, NULL),
(2, 'Matriculado', NULL, NULL),
(3, 'Inactivo', NULL, NULL),
(4, 'Retirado', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `id_types`
--

CREATE TABLE `id_types` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `id_types`
--

INSERT INTO `id_types` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Preinscrito', NULL, NULL),
(2, 'Matriculado', NULL, NULL),
(3, 'Inactivo', NULL, NULL),
(4, 'Retirado', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `levels`
--

CREATE TABLE `levels` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `levels`
--

INSERT INTO `levels` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Básico', NULL, NULL),
(2, 'Intermedio', NULL, NULL),
(3, 'Avanzado', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_11_000000_create_roles_table', 1),
('2014_10_11_000001_create_user_statuses_table', 1),
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2017_06_21_071154_create_id_types_table', 1),
('2018_03_10_045651_create_payment_satuses_table', 1),
('2019_02_05_000001_create_course_categories_table', 1),
('2019_02_05_060210_create_plans_table', 1),
('2019_02_05_062343_create_days_table', 1),
('2019_02_05_062344_create_shedules_table', 1),
('2019_02_05_064200_create_people_table', 1),
('2019_02_05_064201_create_students_table', 1),
('2019_02_05_064202_create_teachers_table', 1),
('2019_02_05_064500_create_courses_table', 1),
('2019_02_05_065030_create_enrolment_statuses_table', 1),
('2019_02_05_065245_create_enrolments_table', 1),
('2019_02_06_054939_create_levels_table', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `payment_satuses`
--

CREATE TABLE `payment_satuses` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `payment_satuses`
--

INSERT INTO `payment_satuses` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Pendiente', NULL, NULL),
(2, 'Pagado', NULL, NULL),
(3, 'Cancelado', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `plans`
--

CREATE TABLE `plans` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `plans`
--

INSERT INTO `plans` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Estándar', NULL, NULL),
(2, 'Intensivo', NULL, NULL),
(3, 'Personalizado', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `name` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'admin', NULL, NULL),
(2, 'profesor', NULL, NULL),
(3, 'estudiante', NULL, NULL),
(4, 'asesor', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `shedules`
--

CREATE TABLE `shedules` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `day` smallint(5) UNSIGNED NOT NULL,
  `time` time NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `shedules`
--

INSERT INTO `shedules` (`id`, `day`, `time`, `created_at`, `updated_at`) VALUES
(1, 1, '08:00:00', NULL, NULL),
(2, 1, '09:00:00', NULL, NULL),
(3, 1, '10:00:00', NULL, NULL),
(4, 1, '11:00:00', NULL, NULL),
(5, 1, '12:00:00', NULL, NULL),
(6, 1, '13:00:00', NULL, NULL),
(7, 1, '14:00:00', NULL, NULL),
(8, 1, '15:00:00', NULL, NULL),
(9, 1, '16:00:00', NULL, NULL),
(10, 1, '17:00:00', NULL, NULL),
(11, 1, '18:00:00', NULL, NULL),
(12, 2, '08:00:00', NULL, NULL),
(13, 2, '09:00:00', NULL, NULL),
(14, 2, '10:00:00', NULL, NULL),
(15, 2, '11:00:00', NULL, NULL),
(16, 2, '12:00:00', NULL, NULL),
(17, 2, '13:00:00', NULL, NULL),
(18, 2, '14:00:00', NULL, NULL),
(19, 2, '15:00:00', NULL, NULL),
(20, 2, '16:00:00', NULL, NULL),
(21, 2, '17:00:00', NULL, NULL),
(22, 2, '18:00:00', NULL, NULL),
(23, 3, '08:00:00', NULL, NULL),
(24, 3, '09:00:00', NULL, NULL),
(25, 3, '10:00:00', NULL, NULL),
(26, 3, '11:00:00', NULL, NULL),
(27, 3, '12:00:00', NULL, NULL),
(28, 3, '13:00:00', NULL, NULL),
(29, 3, '14:00:00', NULL, NULL),
(30, 3, '15:00:00', NULL, NULL),
(31, 3, '16:00:00', NULL, NULL),
(32, 3, '17:00:00', NULL, NULL),
(33, 3, '18:00:00', NULL, NULL),
(34, 4, '08:00:00', NULL, NULL),
(35, 4, '09:00:00', NULL, NULL),
(36, 4, '10:00:00', NULL, NULL),
(37, 4, '11:00:00', NULL, NULL),
(38, 4, '12:00:00', NULL, NULL),
(39, 4, '13:00:00', NULL, NULL),
(40, 4, '14:00:00', NULL, NULL),
(41, 4, '15:00:00', NULL, NULL),
(42, 4, '16:00:00', NULL, NULL),
(43, 4, '17:00:00', NULL, NULL),
(44, 4, '18:00:00', NULL, NULL),
(45, 5, '08:00:00', NULL, NULL),
(46, 5, '09:00:00', NULL, NULL),
(47, 5, '10:00:00', NULL, NULL),
(48, 5, '11:00:00', NULL, NULL),
(49, 5, '12:00:00', NULL, NULL),
(50, 5, '13:00:00', NULL, NULL),
(51, 5, '14:00:00', NULL, NULL),
(52, 5, '15:00:00', NULL, NULL),
(53, 5, '16:00:00', NULL, NULL),
(54, 5, '17:00:00', NULL, NULL),
(55, 5, '18:00:00', NULL, NULL),
(56, 6, '08:00:00', NULL, NULL),
(57, 6, '09:00:00', NULL, NULL),
(58, 6, '10:00:00', NULL, NULL),
(59, 6, '11:00:00', NULL, NULL),
(60, 6, '12:00:00', NULL, NULL),
(61, 6, '13:00:00', NULL, NULL),
(62, 6, '14:00:00', NULL, NULL),
(63, 6, '15:00:00', NULL, NULL),
(64, 6, '16:00:00', NULL, NULL),
(65, 6, '17:00:00', NULL, NULL),
(66, 6, '18:00:00', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `teachers`
--

CREATE TABLE `teachers` (
  `id` int(10) UNSIGNED NOT NULL,
  `persona` int(10) UNSIGNED NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `observations` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `role_id` smallint(5) UNSIGNED NOT NULL,
  `status` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_statuses`
--

CREATE TABLE `user_statuses` (
  `id` tinyint(1) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `courses_course_category_index` (`course_category`),
  ADD KEY `courses_level_index` (`level`),
  ADD KEY `courses_plan_index` (`plan`),
  ADD KEY `courses_shedule_index` (`shedule`),
  ADD KEY `courses_teacher_index` (`teacher`);

--
-- Indices de la tabla `course_categories`
--
ALTER TABLE `course_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `days`
--
ALTER TABLE `days`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `enrolments`
--
ALTER TABLE `enrolments`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `enrolment_statuses`
--
ALTER TABLE `enrolment_statuses`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `id_types`
--
ALTER TABLE `id_types`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `levels`
--
ALTER TABLE `levels`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indices de la tabla `payment_satuses`
--
ALTER TABLE `payment_satuses`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `plans`
--
ALTER TABLE `plans`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `shedules`
--
ALTER TABLE `shedules`
  ADD PRIMARY KEY (`id`),
  ADD KEY `shedules_day_index` (`day`);

--
-- Indices de la tabla `teachers`
--
ALTER TABLE `teachers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `teachers_persona_index` (`persona`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_id_index` (`role_id`),
  ADD KEY `users_status_index` (`status`);

--
-- Indices de la tabla `user_statuses`
--
ALTER TABLE `user_statuses`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `courses`
--
ALTER TABLE `courses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `course_categories`
--
ALTER TABLE `course_categories`
  MODIFY `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `days`
--
ALTER TABLE `days`
  MODIFY `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `enrolments`
--
ALTER TABLE `enrolments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `enrolment_statuses`
--
ALTER TABLE `enrolment_statuses`
  MODIFY `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `id_types`
--
ALTER TABLE `id_types`
  MODIFY `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `levels`
--
ALTER TABLE `levels`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `payment_satuses`
--
ALTER TABLE `payment_satuses`
  MODIFY `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `plans`
--
ALTER TABLE `plans`
  MODIFY `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `roles`
--
ALTER TABLE `roles`
  MODIFY `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `shedules`
--
ALTER TABLE `shedules`
  MODIFY `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;

--
-- AUTO_INCREMENT de la tabla `teachers`
--
ALTER TABLE `teachers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
