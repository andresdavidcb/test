-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 07-03-2018 a las 11:38:24
-- Versión del servidor: 10.1.31-MariaDB
-- Versión de PHP: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `valenci2_academia`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `forms`
--

CREATE TABLE `forms` (
  `id` int(10) NOT NULL,
  `module` int(11) NOT NULL,
  `route` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `model` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `table_label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fields` text COLLATE utf8_unicode_ci NOT NULL,
  `create_fields` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `id_types`
--

CREATE TABLE `id_types` (
  `id` smallint(6) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `id_types`
--

INSERT INTO `id_types` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Tarjeta de identidad', NULL, NULL),
(2, 'Cédula de ciudadanía', NULL, NULL),
(3, 'Pasaporte', NULL, NULL),
(4, 'Cédula de extranjería', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2016_10_28_163646_create_permitions_table', 2),
('2016_10_28_173206_create_modules_table', 2),
('2016_10_28_200220_create_privileges_table', 2),
('2017_06_16_170016_create_roles_table', 3),
('2017_02_18_161910_create_forms_table', 4),
('2017_06_21_053819_create_students_table', 5),
('2017_06_21_071154_create_id_types_table', 6);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `modules`
--

CREATE TABLE `modules` (
  `id` int(10) NOT NULL,
  `title` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `modules`
--

INSERT INTO `modules` (`id`, `title`, `name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'administración', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permitions`
--

CREATE TABLE `permitions` (
  `id` int(10) UNSIGNED NOT NULL,
  `role` int(11) NOT NULL,
  `module` int(11) NOT NULL,
  `privileges` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `privileges`
--

CREATE TABLE `privileges` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `id` int(10) NOT NULL,
  `name` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'admin', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `students`
--

CREATE TABLE `students` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lastname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `id_type` smallint(6) NOT NULL,
  `identification` bigint(20) NOT NULL,
  `birthdate` date NOT NULL,
  `main_phone` bigint(20) NOT NULL,
  `sec_phone` bigint(20) NOT NULL,
  `interest_courses` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `students`
--

INSERT INTO `students` (`id`, `name`, `lastname`, `id_type`, `identification`, `birthdate`, `main_phone`, `sec_phone`, `interest_courses`, `email`, `created_at`, `updated_at`) VALUES
(2, 'Luis felipe', 'Sáez aparicio', 2, 1067916515, '1992-10-12', 3207944050, 3145730610, 'guitarra,canto', 'pipesaez2@hotmail.com', '2018-01-10 19:05:08', '2018-01-10 19:05:08'),
(3, 'Victor Daniel', 'Negrete Baza', 1, 1067865824, '2003-12-19', 3045889142, 3128934257, 'guitarra', 'negretebazavictordaniel@gmail.com', '2018-01-17 00:52:13', '2018-01-17 00:52:13'),
(4, 'lorena', 'vergara', 2, 1102809038, '1987-03-09', 3042132480, 0, 'canto', 'loremaver@gmail.com', '2018-01-25 08:13:28', '2018-01-25 08:13:28'),
(5, 'Isabel', 'Bulasco ', 1, 103577865, '2006-02-07', 3017787102, 7860657, 'guitarra,canto', 'Isabel020706@hotmail.com', '2018-01-25 21:30:30', '2018-01-25 21:30:30'),
(6, 'pedro luis', 'trujillo rios ', 2, 1233343079, '1998-09-28', 3116105143, 347953927, 'guitarra,canto', 'wplay123@outlook.com', '2018-01-29 23:22:08', '2018-01-29 23:22:08'),
(8, 'Jhan Carlos', 'Viloria Murillo', 2, 1007781249, '0000-00-00', 3215012831, 3215649925, 'batería', 'jhanviloria45@gmail.com', '2018-02-05 21:52:38', '2018-02-05 21:52:38'),
(9, 'Jonathan', 'Dix Polo', 1, 1002998196, '0000-00-00', 7866062, 0, 'piano,canto', 'jonathandix12@gmail.com', '2018-02-08 18:09:56', '2018-02-08 18:09:56'),
(10, 'Yulisa', 'Villegas Ramos', 2, 1067956302, '1997-08-06', 3104244820, 0, 'piano', 'yulisa997@hotmail.es', '2018-02-10 00:32:21', '2018-02-10 00:32:21'),
(11, 'juan sebastian ', 'ballen manosalva', 2, 1116807689, '1997-12-03', 3184677781, 3165462227, 'guitarra', 'sebastianballenm@hotmail.com', '2018-02-17 02:30:51', '2018-02-17 02:30:51'),
(12, 'Daniel de jesus', 'Holguin Ricardo', 2, 1063294830, '1993-09-10', 3122493144, 3103753884, 'guitarra,canto', 'daniel.hr1@hotmail.com', '2018-02-20 03:57:02', '2018-02-20 03:57:02'),
(13, 'Marcela', 'Martinez Babilonia', 2, 1007339981, '1995-08-19', 3126010564, 0, 'guitarra,canto', 'babiloniatheblackrose@gmail.com', '2018-02-20 21:37:12', '2018-02-20 21:37:12'),
(14, 'Luciano ', 'Ferreras', 1, 37369943, '1994-04-10', 542215027524, 0, 'batería', 'lucianoferrerasmdh@gmail.com', '2018-02-22 11:55:38', '2018-02-22 11:55:38'),
(15, 'Isaid joseth', 'Martinez martinez', 2, 1102840883, '1992-07-16', 3216515130, 0, 'guitarra,piano,canto', 'isaidmarti7@hotmail.com', '2018-02-26 02:01:04', '2018-02-26 02:01:04'),
(16, 'Magnoria', 'Gomez Plata', 2, 37948782, '1982-10-08', 3145639249, 0, 'piano', 'margy-08@hotmail.com', '2018-03-01 21:32:52', '2018-03-01 21:32:52'),
(18, 'Moisés David', 'Guzmán Pretel', 2, 1067938918, '1995-02-26', 3013663186, 3013663186, 'guitarra,canto', 'moisesdavid0010@gmail.com', '2018-03-07 07:26:28', '2018-03-07 07:26:28');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Gabriel Coronado', 'gabrielcb_007@gmail.com', '$2y$10$JSpQyb/FNfC41qtYDr24aO2/62kqALV1ZPc4ijNR/DuUxZFfEooOi', 'mZyWFOvXcANXI61NjzuFP1i7bkf0LMAeEKXu41JwICGothpZPq7a1HJMbu2N', '2017-05-24 21:54:20', '2017-06-21 10:27:42');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `forms`
--
ALTER TABLE `forms`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `id_types`
--
ALTER TABLE `id_types`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `modules`
--
ALTER TABLE `modules`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indices de la tabla `permitions`
--
ALTER TABLE `permitions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permitions_role_index` (`role`),
  ADD KEY `permitions_module_index` (`module`),
  ADD KEY `permitions_privileges_index` (`privileges`);

--
-- Indices de la tabla `privileges`
--
ALTER TABLE `privileges`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_type` (`id_type`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `forms`
--
ALTER TABLE `forms`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `id_types`
--
ALTER TABLE `id_types`
  MODIFY `id` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `modules`
--
ALTER TABLE `modules`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `permitions`
--
ALTER TABLE `permitions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `privileges`
--
ALTER TABLE `privileges`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `students`
--
ALTER TABLE `students`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
