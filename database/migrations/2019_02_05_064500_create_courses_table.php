<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',40);
            $table->unsignedSmallInteger('course_category')->index();
            $table->unsignedSmallInteger('level')->index();
            $table->unsignedSmallInteger('plan')->index();
            $table->decimal('price',12,2);
            $table->smallinteger('shedule')->unsigned()->index();
            $table->integer('teacher')->unsigned()->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('courses');
    }
}
