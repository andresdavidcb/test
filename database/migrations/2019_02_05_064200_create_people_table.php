<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePeopleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('people', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('lastname');
            $table->unsignedSmallInteger('id_type')->index();
            $table->biginteger('identification')->unique();
            $table->date('birthdate');
            $table->biginteger('main_phone');
            $table->biginteger('sec_phone')->nullable();
            $table->string('email');
            $table->string('observations');
            $table->string('interest_courses')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('people');
    }
}
