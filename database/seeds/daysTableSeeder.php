<?php

use Illuminate\Database\Seeder;

class daysTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $table='days';
        $items=['Lunes','Martes','Miércoles','Jueves','Viernes','Sábado'];

        foreach ($items as $item) {
            DB::table($table)->insert(['name' => $item ]);
        }
    }
}
