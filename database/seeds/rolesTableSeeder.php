<?php

use Illuminate\Database\Seeder;


class rolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $table='roles';    
        $items=['admin','profesor','estudiante','asesor'];
        foreach ($items as $item) {
            DB::table($table)->insert(['name' => $item ]);
        }
       
    }
}
