<?php

use Illuminate\Database\Seeder;

class enrolmentStatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       	$table='enrolment_statuses';
        $items=['Preinscrito','Matriculado','Inactivo','Retirado'];

        foreach ($items as $item) {
            DB::table($table)->insert(['name' => $item ]);
        }
    }
}
