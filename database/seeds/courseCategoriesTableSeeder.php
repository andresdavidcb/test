<?php

use Illuminate\Database\Seeder;

class courseCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       
        $table='course_categories';
        $items=['Piano','Guitarra','Saxofón','Vocalización','Bajo','Batería','Acordeón'];

        foreach ($items as $item) {
            DB::table($table)->insert(['name' => $item ]);
        }
	   
    }
}
