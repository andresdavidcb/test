<?php

use Illuminate\Database\Seeder;

class paymentStatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $table='payment_satuses';
        $items=['Pendiente','Pagado','Cancelado',];

        foreach ($items as $item) {
            DB::table($table)->insert(['name' => $item ]);
        }
    }
}
