<?php

use Illuminate\Database\Seeder;

class idTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $table='id_types';
        $items=['Cédula de ciudadanía','Tarjeta de identidad','Pasaporte','Cédula de extranjería','Registro civil'];

        foreach ($items as $item) {
            DB::table($table)->insert(['name' => $item ]);
        }
    }
}
