<?php

use Illuminate\Database\Seeder;

class shedulesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $table='shedules';
        //$items=['Preinscrito','Matriculado','Inactivo','Retirado'];
        $day=1;
        while ($day<7) {
        	for($time=8;$time<19;$time++){
        			DB::table($table)->insert(['day' => $day,'time' => $time.":00:00" ]);
        	}
        	$day++;	
        }
            
        
    }
}
