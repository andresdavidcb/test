<?php

use Illuminate\Database\Seeder;

class plansTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $table='plans';
        $items=['Estándar','Intensivo','Personalizado'];

        foreach ($items as $item) {
            DB::table($table)->insert(['name' => $item ]);
        }
    }
}
