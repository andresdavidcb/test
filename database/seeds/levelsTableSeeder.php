<?php

use Illuminate\Database\Seeder;

class levelsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $table='levels';
        $items=['Básico','Intermedio','Avanzado'];

        foreach ($items as $item) {
            DB::table($table)->insert(['name' => $item ]);
        }
    }
}
