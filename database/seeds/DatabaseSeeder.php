<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(courseCategoriesTableSeeder::class);
        $this->call(daysTableSeeder::class);
        $this->call(enrolmentStatusesTableSeeder::class);
        $this->call(idTypesTableSeeder::class);
        $this->call(levelsTableSeeder::class);
        $this->call(paymentStatusesTableSeeder::class);
        $this->call(plansTableSeeder::class);
        $this->call(rolesTableSeeder::class);
        $this->call(shedulesTableSeeder::class);
        
    }
}
