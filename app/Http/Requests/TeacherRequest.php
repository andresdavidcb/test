<?php

namespace oasis\Http\Requests;

use oasis\Http\Requests\Request;

class TeacherRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required|min:3',
            'lastname'=>'required|min:3',
            'identification'=>'required|unique:teachers,identification|min:7',
            'main_phone'=>'required|min:7',
            'email'=>'email',
            //'interest_courses[]'=>'required',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'El nombre es obligatorio',
            'name.min' => 'El nombre escrito no es válido',
            'lastname.required' => 'El apellido es obligatorio',
            'lastname.min' => 'El apellido digitado no es válido',
            'identification.required' => 'El número e identificación es obligatorio',
            'identification.min' => 'El número digitado no es válido',
            'identification.integer' => 'El número digitado no es válido, no ingrese signos ni letras',
            'identification.unique' => 'Esta identificación ya se encuentra registrada',
            'email.email' => 'Esta no es una cuenta de correo válida',
            'main_phone.min' => 'Número no válido',
            'main_phone.required' => 'Número no válido',
            //'interest_courses[].required' => 'Debe seleccionar uno o varios cursos de su interés',

        ];
    }
}
