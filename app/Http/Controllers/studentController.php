<?php

namespace oasis\Http\Controllers;

use Illuminate\Http\Request;
use oasis\Http\Requests;
use oasis\Http\Requests\studentRequest;


use oasis\User;
use oasis\Models\student;
use oasis\Models\person;
use oasis\Models\id_type;
use oasis\Models\course_category;

use Mail;
use Session;
use Redirect;
class studentController extends Controller
{
    public function index()
    {
        $id_type=id_type::all();
        $course_category=course_category::all();
        return view('signup.index',compact('id_type','course_category'));
    }

    public function store(Request $request)
    {
           
        //person::create($request->all());
        $i=(count($request->interest_courses)>0)?implode(',', $request->interest_courses):'';
        person::create([
            'name' => $request->name,
            'lastname' => $request->lastname,
            'id_type' => $request->id_type,
            'identification' => $request->identification,
            'birthdate' => $request->birthdate,
            'main_phone' => $request->main_phone,
            'sec_phone' => $request->sec_phone,
            'email' => $request->email,
            'observations' => $request->observations,
            'interest_courses' => $i

        ]);
        $student=person::where('identification',$request->identification)->first();
        
        student::create([
            'person' => $student->id,
            'created_by'=>'0',
            'interest_courses'=>$i
        ]); 

        $data['email'] = $request->email;
        $data['name'] = $request->name;

        Mail::send('signup.notification', ['data' => $data], function ($m) use ($data) {
            $m->from('notificaciones@oasisacademia.com', 'OASIS ACADEMIA DE MÚSICA');
            $m->to($data['email'], $data['name'])
            ->subject('Inscripción a OASIS MUSIC');
        });

        Mail::send('signup.notification_admin', ['data' => $data], function ($m) use ($data) {
            $m->from('notificaciones@oasisacademia.com', 'OASIS ACADEMIA DE MÚSICA');
            $m->to('oasisacademiademusica@gmail.com')
            ->subject('Inscripción a OASIS MUSIC');
        });

        Session::put('msg_signup','Gracias por inscribirte, Por favor revisa tu correo electrónico');
       return redirect('signup');

    }
}
