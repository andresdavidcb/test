<?php

namespace oasis\Http\Controllers;

use Illuminate\Http\Request;

use oasis\Http\Requests;

use oasis\Models\person;
use oasis\Models\Payment;
use oasis\Models\PaymentButton;
use Mail;
use Session;
use Redirect;
use Carbon\Carbon;

class paymentRequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $paymentDetails=PaymentButton::all();
        return view('mails.paymentRequest',compact('paymentDetails'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $people=person::where('email',$request->email)->first();
        $paymentDetail=PaymentButton::find($request->paymentDetails);
        $payment=Payment::create([
            'ref'=>$paymentDetail->referenceCode,
            'people_id'=>$people->id,
            'button_id'=>$paymentDetail->id,
            'description'=>$paymentDetail->description,
            'value'=>$paymentDetail->amount,
        ]);
        $data['transaction'] = $payment->id;    
        $data['email'] = $request->email;
        $data['name'] = $people->name;
        Mail::send('mails.sendPayment', ['data' => $data], function ($m) use ($data) {
            $m->from('notificaciones@oasisacademia.com', 'OASIS ACADEMIA DE MÚSICA');
            $m->to($data['email'], $data['name'])
            ->subject('Solicitud de pago OASIS MUSIC');
        });
        return redirect('paymentRequest');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
