<?php

namespace oasis\Http\Controllers;

use Illuminate\Http\Request;
use oasis\Http\Requests;
use oasis\Http\Requests\FormRequest;

use oasis\User;
use oasis\Models\Form;
use oasis\Models\module;


class FormController extends Controller
{
     public function index(Request $request)
    {
         
        $forms=Form::name()->get();
        return view('forms.index',compact('forms'));
    }

    public function create(Request $request)
    {
        $module=module::all();
        return view('forms.create',compact('module'));
    }

    public function store(FormRequest $request)
    {
    	
        $module=module::all();
    	$request->user()->forms()->create([
            'module' => $request->module,
            'route' => $request->route,
            'model' => $request->model,
            'table_label' => $request->table_label,
            'fields' => $request->fields,
            'create_fields' => $request->create_fields,
            'user_id' => $request->user()->id
            ]);

        return view('forms.create-view-forms',compact('module'));
    }

    public function show()
    {

    }

    public function edit(Request $request,$id)
    {
        $form=form::find($id);
        $module=module::all();
        return view('forms.edit',compact('form','module'));
    }

    public function update(Request $request, $id)
    {
        $form=form::find($id);
        $module=module::all();
        $form->fill($request->all());
        $form->save();
        return view('forms.create-view-forms',compact('module'));
        //return redirect('forms');
    }

    public function destroy($id)
    {
        
    }
}
