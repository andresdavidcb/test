<?php

namespace oasis\Http\Controllers;

use Illuminate\Http\Request;

use oasis\Http\Requests;
use oasis\Http\Requests\TeacherRequest;

use oasis\User;
use oasis\Models\teacher;
use oasis\Models\id_type;
use oasis\Models\course_category;
use oasis\Models\person;

use Mail;
use Session;
use Redirect;

class teachersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $teachers=teacher::name()->get();
        $id_types=id_type::all();
        $course_categories=course_category::all();
        $people=person::all();
        //return $students;
        return [
            'teachers'=>$teachers,
            'id_types'=>$id_types,
            'course_categories'=>$course_categories,
            'people'=>$people
        ];
    }

    public function store(Request $request)
    {
       person::create($request->all());
       $item=person::where('identification',$request->identification)->first();
       teacher::create([
                'person' => $item->id,
                //'created_by'=>$request->user()->id
            ]);
       return;
       User::create([
            'name'=>$request->name,
            'email'=>$request->email,
            'role'=>'2',
            'password'=>bcrypt($request->identification)
       ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $teacher=teacher::name()->where('teachers.id',$id)->get();
        return view('teachers.details',compact('teacher'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        person::find($id)->update($request->all());
        return;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
