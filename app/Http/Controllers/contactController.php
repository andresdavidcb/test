<?php

namespace oasis\Http\Controllers;

use oasis\Http\Requests;
use Illuminate\Http\Request;
use Mail;
use Session;
use Redirect;

class contactController extends Controller
{
    public function index()
    {
        return view('contact.index');
    }

    public function store(Request $request)
    {
        
        /*Mail::send('contact.notification',$request->all(),function($msg){
        	$msg->subject('Notificación de correo');
        	$msg->from('contacto@oasisacademia.com');
        	$msg->to('contacto@oasisacademia.com');
        });*/
		Session::flash('message','Mensaje enviado correctamente');        

        return Redirect::to('/contact');
    }
}
