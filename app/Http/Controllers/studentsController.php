<?php

namespace oasis\Http\Controllers;

use Illuminate\Http\Request;

use oasis\Http\Requests;
use oasis\Http\Requests\studentRequest;

use oasis\User;
use oasis\Models\student;
use oasis\Models\id_type;
use oasis\Models\course_category;
use oasis\Models\person;
use oasis\Models\interest_course;

use Mail;
use Session;
use Redirect;

class studentsController extends Controller
{
    public function index()
    {
    	//return "it works";
        $students=student::name()->get();
        $id_types=id_type::all();
        $course_categories=course_category::all();
        $people=person::all();
        //return $students;
        return [
            'students'=>$students,
            'id_types'=>$id_types,
            'course_categories'=>$course_categories,
            'people'=>$people
        ];
    	
    }
    /*public function show($id){
        return $id;
    }*/
    public function store(Request $request)
    {
       person::create($request->all());
       $student=person::where('identification',$request->identification)->first();
       student::create([
                'person' => $student->id,
                'created_by'=>$request->user()->id
            ]);
       User::create([
            'name'=>$request->name,
            'email'=>$request->email,
            'role'=>'3',
            'password'=>bcrypt($request->identification)
       ]);
       return;
    }

    public function update(Request $request, $id)
    { 
        person::find($id)->update($request->all());
        return;
    }

    public function enrolment($id){
        student::find($id)->update(['enrolment_status'=>'2']);
    }
    
    public function destroy($id){
        $student=student::find($id);
        $student->delete();
        return redirect('/students');
    }

}
