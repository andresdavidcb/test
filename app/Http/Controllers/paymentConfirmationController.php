<?php

namespace oasis\Http\Controllers;

use Illuminate\Http\Request;

use oasis\Http\Requests;
use oasis\Models\person;
use oasis\Models\Payment;
use oasis\Models\PaymentButton;

class paymentConfirmationController extends Controller
{
 
    public function index(Request $request)
    {   
        $id=$request->transaction;
        $payment=Payment::find($id);
        $paymentDetail=PaymentButton::find($payment->button_id);
        $person = person::find($payment->people_id);
        // return $payment->id;
        return view('payments.confirmation',compact('payment','paymentDetail','person'));
    }

    public function success()
    {
        $status="Su pago ha sido exitoso";
        return view('payments.status',compact('status'));
    }

    public function rejected()
    {
        $status="Su pago ha sido rechazado";
        return view('payments.status',compact('status'));
    }

    public function proccesed()
    {
        $status="Su pago está pendiente por confirmar";
        return view('payments.status',compact('status'));
    }


}
