<?php

namespace oasis\Http\Controllers;

use Illuminate\Http\Request;

use oasis\Http\Requests;

class listController extends Controller
{
    public function students(){
    	
    	return view('students.index');
    }
    public function teachers(){
    	
    	return view('teachers.index');
    }
    public function assesors(){
    	
    	return view('assesors.index');
    }
    public function courses_shedule(){
    	
    	return view('courses_shedule.index');
    }
}
