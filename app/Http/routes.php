<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::auth();



Route::get('/home', 'HomeController@index');
Route::get('/aboutus', 'aboutusController@index');
Route::get('/courses', 'coursesController@index');
Route::resource('contact', 'contactController');
Route::resource('signup', 'studentController');
Route::resource('signup2', 'student2Controller');
Route::resource('interest_courses', 'interest_courses');
Route::get('paymentConfirmation', 'paymentConfirmationController@index');
Route::get('paymentSuccess', 'paymentConfirmationController@success');
Route::get('paymentRejected', 'paymentConfirmationController@rejected');
Route::get('paymentProcessed', 'paymentConfirmationController@proccesed');



Route::group(['middleware' => ['auth','admin']], function () {
    Route::resource('paymentRequest','paymentRequestController');

    /* Route::resource('forms','FormController');
    Route::get('studentslist','listController@students');
    Route::get('teacherslist','listController@teachers');
    Route::get('asessorslist','listController@assesors');
    Route::get('courses_shedule','listController@courses_shedule');
    //Route::resource('people','peopleController');
    Route::resource('students','studentsController');
    Route::get('students/enrolment/{id}','studentsController@enrolment');
    Route::resource('teachers','teachersController');
    Route::resource('courses_shedule','courses_sheduleController');
    Route::resource('enrolments','enrolmentController');
    //Route::get('students/enrolment/{id}','studentsController@enrolment'); */
});

Route::group(['middleware' => ['auth','assesor']], function () {
    
    Route::get('studentslist','listController@students');
    Route::resource('students','studentsController',['except'=>'create','edit','update']);
    //Route::get('students/enrolment/{id}','studentsController@enrolment');
});