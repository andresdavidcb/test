<?php

namespace oasis\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class assesorMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::User()->role==4 && Auth::User()->status==1)
        return $next($request);
        else abort(403);
    }
}
