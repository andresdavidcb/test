<?php

namespace oasis\Models;

use Illuminate\Database\Eloquent\Model;

class teacher extends Model
{
    protected $table="teachers";
    protected $fillable = [
        'person',
		];

    public function scopeName($query){
    	$query->select('teachers.id','person','people.lastname as lastname','id_types.name as id_type','id_types.id as id_type_key','people.identification as identification',
        'people.name as name','people.main_phone as main_phone','people.sec_phone as sec_phone','people.email as email','teachers.created_at as created_date','observations')
        ->join('people','people.id','=','teachers.person')
        ->join('id_types','id_types.id','=','people.id_type');
    }

    public function user()
	{
		return $this->bellongsTo(User::class);
	}

    	public function id_type()
	{
		return $this->bellongsTo(id_type::class);
	}
}
