<?php

namespace oasis\Models;

use Illuminate\Database\Eloquent\Model;

class PaymentButton extends Model
{
    protected $table = "payment_buttons";

    protected $fillable = [
        'buttonId', 'description', 'referenceCode', 'amount','signature'
    ];
}
