<?php

namespace oasis\Models;

use Illuminate\Database\Eloquent\Model;

class role extends Model
{
     protected $table = 'roles';
      protected $fillable=['name'];

    public function users()
    {
       return $this->hasMany(User::class);
    }
    
}
