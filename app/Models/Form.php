<?php

namespace oasis\Models;

use Illuminate\Database\Eloquent\Model;

class Form extends Model
{
    protected $table='forms';
    protected $fillable=['id','module','route','model','table_label','fields','create_fields'];

    public function scopeName($query){
        $query->select('modules.name as module','route','model','table_label','fields','create_fields','forms.id')
        ->join('modules','modules.id','=','forms.module');
    }

    public function module()
	{
		return $this->bellongsTo(module::class);
	}

    public function user()
    {
        return $this->bellongsTo(User::class);
    }
}
