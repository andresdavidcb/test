<?php

namespace oasis;

use Illuminate\Foundation\Auth\User as Authenticatable;



class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function role()
    {
        //return $this->bellongsTo(role::class);
    }
     public function user_status()
    {
        return $this->bellongsTo(user_status::class);
    }
    public function forms()
    {
        return $this->hasMany(Form::class);
    }
    public function enrolments(){
        return $this->hasMany(enrolment::class);
    }
    public function students(){
        return $this->hasMany(students::class);
    }
    public function teachers(){
        return $this->hasMany(teacher::class);
    }
}
