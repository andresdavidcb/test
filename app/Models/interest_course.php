<?php

namespace oasis\Models;

use Illuminate\Database\Eloquent\Model;

class interest_course extends Model
{
    protected $table="interest_courses";
    //protected $fillable=['course','student'];

    public function student()
	{
		return $this->bellongsTo(student::class);
	}
	public function course_category()
	{
		return $this->bellongsTo(course_category::class);
	}
}
