<?php

namespace oasis\Models;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $table = "payments";

    protected $fillable = [
        'people_id','button_id','ref', 'value','description', 'status',
    ];
}
