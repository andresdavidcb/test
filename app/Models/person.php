<?php

namespace oasis\Models;

use Illuminate\Database\Eloquent\Model;

class person extends Model
{
    protected $table = "people";

    protected $fillable = [
        'name','lastname', 'id_type','identification', 'birthdate','main_phone','sec_phone','email','observations','interest_courses','attendant'
    ];

    public function scopeName($query){
    	$query->select('people.id as id','people.name as name', 'lastname','id_type',
    		'identification','birthdate','main_phone','sec_phone','email','people.created_at as created_date','observations');
    }

    public function user()
	{
		return $this->bellongsTo(User::class);
	}

    public function enrolments()
    {
        return $this->hasMany(enrolment::class);
    }

	public function id_type()
	{
		return $this->bellongsTo(id_type::class);
	}

}
