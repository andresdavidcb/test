<?php

namespace oasis\Models;

use Illuminate\Database\Eloquent\Model;

class course extends Model
{
	protected $table="courses";

	public function enrolments(){
		return $this->hasMany(enrolment::class);
	}
   	public function course_category()
      {
      	return $this->bellongsTo(course_category::class);
      }

    public function level()
      {
      	return $this->bellongsTo(level::class);
      }

    public function plan()
      {
      	return $this->bellongsTo(plan::class);
      }  

}
