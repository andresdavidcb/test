<?php

namespace oasis\Models;

use Illuminate\Database\Eloquent\Model;

class student extends Model
{
    protected $table="students";
    protected $fillable = [
        'interest_courses','person','created_at'
    ];

    public function scopeName($query){
    	$query->select('students.id','person','people.lastname as lastname','id_types.name as id_type','id_types.id as id_type_key','people.identification as identification',
        'people.name as name','people.main_phone as main_phone','people.sec_phone as sec_phone','people.email as email','interest_courses','students.created_at as created_date','enrolment_statuses.name as status','observations')
        ->join('people','people.id','=','students.person')
        ->join('enrolment_statuses','enrolment_statuses.id','=','students.enrolment_status')
        ->join('id_types','id_types.id','=','people.id_type');

    }
    
    public function user()
	{
		return $this->bellongsTo(User::class);
	}

    public function enrolments()
    {
        return $this->hasMany(enrolment::class);
    }

	public function id_type()
	{
		return $this->bellongsTo(id_type::class);
	}

    public function interest_courses()
    {
        return $this->hasMany(interest_course::class);
    }

}
/*
public function musical_courses()
{

}*/
