<?php

namespace oasis\Models;

use Illuminate\Database\Eloquent\Model;

class user_status extends Model
{
    protected $table='user_statuses';

     public function users()
    {
       return $this->hasMany(User::class);
    }
}
