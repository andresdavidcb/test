<?php

namespace oasis\Model;

use Illuminate\Database\Eloquent\Model;

class enrolment extends Model
{
    protected $table="enrolments";
    protected $filable=['course'];

    public function scopeName($query){
    	$query->select('enrolments.id as registration','lastname','id_types.name as id_type','identification',
    		'students.name as firstname','courses.name as course','courses.standar_price as s_price',
    		'payment_statuses.name as payment_status')
        ->join('students','students.id','=','enrolments.student')
        ->join('courses','courses.id','=','enrolments.course')
        ->join('payment_statuses','payment_statuses.id','=','enrolments.payment_status')
        ->join('id_types','id_types.id','=','students.id_type');
    }

    public function course(){
    	return $this->bellongsTo(course::class);
    }
    public function student(){
    	return $this->bellongsTo(student::class);
    }
    public function payment_status(){
    	return $this->bellongsTo(payment_status::class);
    }

}
