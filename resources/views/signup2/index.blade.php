@extends('layouts.app')
@section('content')
<h1>DATOS PERSONALES</h1>
<div class='container col-md-12' id="main">
{!! Form::open(['route' => 'signup.store','method' => 'POST']) !!}	
<!--<form method="post" v-on:submit.prevent="createItem">-->
	<div class="col-md-6 form-group{{ $errors->has('name') ? ' has-error' : '' }}">
	    <label for='name' class='col-md-12 control-label'>Nombres*</label>
	    <div class='col-md-12'>
	    <input required type='text' id='name' name='name' v-model="c.name" class='form-control' value="{{old('name')}}">
	     @if ($errors->has('name'))
	        <span class='help-block'><strong>{{ $errors->first('name') }}</strong></span>
	    @endif
	    </div>
	</div>

	<div class="col-md-6 form-group{{ $errors->has('lastname') ? ' has-error' : '' }}">
	    <label for='lastname' class='col-md-12 control-label'>Apellidos*</label>
	    <div class='col-md-12'>
	    <input required type='text' id='lastname' name='lastname' v-model="c.lastname" class='form-control' value="{{old('lastname')}}">
	     @if ($errors->has('lastname'))
	        <span class='help-block'><strong>{{ $errors->first('lastname') }}</strong></span>
	    @endif
	    </div>
	</div>

	<div class="col-md-6 form-group{{ $errors->has('id_type') ? ' has-error' : '' }}">
	    <label for='id_type' class='col-md-12 control-label'>Tipo de documento</label>
	    <div class='col-md-12'>
	   <select name='id_type' v-model="c.id_type" class='form-control'>
	            @foreach($id_type as $field)
	            @if($field->id==old('id_type'))
	            <option value="{{$field->id}}" selected>
	            @else
	            <option value="{{$field->id}}">
	            @endif   
	                {{$field->name}}</option>
	            @endforeach
	       </select>
	          @if ($errors->has('id_type'))
	        <span class='help-block'><strong>{{ $errors->first('id_type') }}</strong></span>
	    @endif
	    </div>
	</div>


	<div class="col-md-6 form-group{{ $errors->has('identification') ? ' has-error' : '' }}">
	    <label for='identification' class='col-md-12 control-label'>Identificación*</label>
	    <div class='col-md-12'>
	    <input required type='text' id='identification' name='identification' v-model="c.identification" class='form-control' value="{{old('identification')}}">
	     @if ($errors->has('identification'))
	        <span class='help-block'><strong>{{ $errors->first('identification') }}</strong></span>
	    @endif
	    </div>
	</div>

	<div class="col-md-6 form-group{{ $errors->has('birthdate') ? ' has-error' : '' }}">
	    <label for='birthdate' class='col-md-12 control-label'>Fecha de nacimiento</label>
	    <div class='col-md-12'>
	    <input type='date' id='birthdate' name='birthdate' v-model="c.birthdate" class='form-control' value="{{isset($form)?$form->birthdate:old('birthdate')}}">
	     @if ($errors->has('birthdate'))
	        <span class='help-block'><strong>{{ $errors->first('birthdate') }}</strong></span>
	    @endif
	    </div>
	</div>

	<div class="col-md-6 form-group{{ $errors->has('email') ? ' has-error' : '' }}">
	    <label for='email' class='col-md-12 control-label'>Correo electrónico*</label>
	    <div class='col-md-12'>
	    <input required type='email' id='email' name='email' v-model="c.email" class='form-control' value="{{old('email')}}">
	     @if ($errors->has('email'))
	        <span class='help-block'><strong>{{ $errors->first('email') }}</strong></span>
	    @endif
	    </div>
	</div>

	<div required class="col-md-6 form-group{{ $errors->has('main_phone') ? ' has-error' : '' }}">
	    <label for='main_phone' class='col-md-12 control-label'>Teléfono principal*</label>
	    <div class='col-md-12'>
	    <input type='number' id='main_phone' name='main_phone' v-model="c.main_phone" class='form-control' value="{{old('main_phone')}}">
	     @if ($errors->has('main_phone'))
	        <span class='help-block'><strong>{{ $errors->first('main_phone') }}</strong></span>
	    @endif
	    </div>
	</div>

	<div class="col-md-6 form-group{{ $errors->has('sec_phone') ? ' has-error' : '' }}">
	    <label for='sec_phone' class='col-md-12 control-label'>Teléfono alternativo</label>
	    <div class='col-md-12'>
	    <input type='number' id='sec_phone' name='sec_phone' v-model="c.sec_phone" class='form-control' value="{{old('sec_phone')}}">
	     @if ($errors->has('sec_phone'))
	        <span class='help-block'><strong>{{ $errors->first('sec_phone') }}</strong></span>
	    @endif
	    </div>
	</div>
	<div class="col-md-6 form-group{{ $errors->has('attendant') ? ' has-error' : '' }}">
	    <label for='attendant' class='col-md-12 control-label'>Acudiente (Si es menor de edad)</label>
	    <div class='col-md-12'>
	    <input type='text' id='attendant' name='attendant' v-model="c.attendant" class='form-control' value="{{old('attendant')}}">
	     @if ($errors->has('attendant'))
	        <span class='help-block'><strong>{{ $errors->first('attendant') }}</strong></span>
	    @endif
	    </div>
	</div>
	
	<div class="col-md-6 form-group{{ $errors->has('interest_courses') ? ' has-error' : '' }}">
	    <label for='interest_courses' class='col-md-12 control-label'>Cursos de interes*</label>
	    <div class='col-md-12'>

		    @foreach($course_category as $field)
            <div class='col-md-4'>
                <input class='' name='interest_courses[]' v-model="interest_courses" type='checkbox' value='{{$field->name}}'>{{$field->name}}
            </div>
            @endforeach
            
		    @if ($errors->has('interest_courses[]'))
		    	<span class='help-block'><strong>{{ $errors->first('interest_courses[]') }}</strong></span>
		    @endif
	    </div>	     
	</div>

	

	
<div class='col-md-12' style="text-align:center">
<button type='submit' class='btn btn-danger'>
    Registrarme <i class='glyphicon glyphicon-pencil'></i>
</button>


<br><br>
</div>



<div class='success'>
	{{Session::get('msg_signup')}}
	{{Session::forget('msg_signup')}}
</div>
{!! Form::close() !!}
<!--</form>-->


</div>
<!--
<script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.5.17/vue.js" ></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.18.0/axios.min.js" ></script>
<script src="{{asset('js/signup.js')}}"></script>
-->
@endsection