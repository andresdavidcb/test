<!DOCTYPE html>

<?php
$url="http://oasisacademia.com/";
?>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>OASIS ACADEMIA DE MÚSICA</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">

    <!-- Styles -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
     <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
    {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}
    <link rel="stylesheet" type="text/css" href="{{$url}}/css/main.css">

    
    
</head>
<body id="app-layout">
<img src="img/gallery/building.jpg" width="100%">
<!--
    <nav class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">

               
                <button style="border:none;background:none" type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <img src="{{$url}}/img/style/menu_mobile.png">
                </button>

               
                <a href="{{ url('/') }}">
                    <img src="{{$url}}/img/style/logo.png" class"logo">
                </a>

            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                
                <ul class="nav navbar-nav">
                     
                    @if (Auth::check())
                    <li><a href="{{ url('studentslist') }}">Personal</a></li>
                    @endif
                   
                    
                </ul>
                
                <ul class="nav navbar-nav navbar-right">!-->                    

                    <?php 
                    /*$url=$_SERVER['REQUEST_URI'];
                    $a='active';*/
                    ?>
                    

                    <!--@if (Auth::guest())

                        <li><a href="{{ url('/') }}" class="{{$url=='/'?$a:''}}">INICIO</a></li>
                        <li><a href="{{ url('aboutus') }}" class="{{$url=='aboutus'?$a:''}}">¿QUIÉNES SOMOS?</a></li>
                        <li><a href="{{ url('courses') }}" class="{{$url=='courses'?$a:''}}">NUESTROS CURSOS</a></li>
                        <li><a href="{{ url('contact') }}" class="{{$url=='contact'?$a:''}}">CONTACTO</a></li>
                        
                        <li><a href="{{ url('signup') }}" class="{{$url=='signup'?$a:''}}">INSCRÍBETE</a></li>
                        <li><a href="{{ url('login') }}" class="{{$url=='signup'?$a:''}}"><i class="glyphicon glyphicon-user"></i></a></li>
                        
                    @else
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Salir</a></li>
                            </ul>
                        </li>
                    @endif
                </ul>
                
            </div>
        </div>
    </nav>-->

    @yield('content')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js" integrity="sha384-I6F5OKECLVtK/BL+8iSLDEHowSAfUo76ZL9+kGAgTRdiByINKJaqTPH/QVNS1VDb" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
    
</script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    <script src="js/search.js"></script>
    
    

@if(Auth::guest())
<footer>
&copy; 2019 Oasis Music | Todos los derechos reservados | Webmaster: Andrés David Coronado Blanco<br>
<a href="http://facebook.com/oasisacademiademusica" target="_blank"><img src="img/style/facebook.png"></a>
    <a href="http://instagram.com/oasisacademiademusica" target="_blank"><img src="img/style/instagram.png"></a>
    <a href="https://www.youtube.com/channel/UCLcCfm5o5PL4AS8UishkzQw" target="_blank"><img src="img/style/youtube.png"></a>

</footer>  
@endif()  
</body>
</html>
