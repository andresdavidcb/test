<script src="scripts/search.js"></script>

<?php $url= $_SERVER['REQUEST_URI'];
$thislink=explode('/',$url);
$a= $thislink[1];
$active="active";
?>

<div class="list-group col-md-2">
    
  <a href="{{ url('studentslist') }}" class="list-group-item {{$a=='studentslist'?$active:''}}">Estudiantes</a>	
  <a href="{{ url('teacherslist') }}" class="list-group-item {{$a=='teacherslist'?$active:''}}">Profesores</a>	
  <a href="{{ url('assesorslist') }}" class="list-group-item {{$a=='assesorslist'?$active:''}}">Asesores</a>	
  <a href="{{ url('courses_shedulelist') }}" class="list-group-item {{$a=='courses_shedulelist'?$active:''}}">Cursos</a>	
</div>