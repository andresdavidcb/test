@extends('layouts.app')
@section('content')
@include('layouts.admin')
<div class='container col-md-10'>
    <div class='row'>
            <div class='panel panel-default'>
                <div class='panel-heading'><b>Edición de formulario</b> <span class='glyphicon glyphicon-arrow-right'></span> {{$form->route}}</div>
                <div class='panel-body'>
                    {!! Form::model($form,['route' => ['forms.update',$form->id],'method'=>'PUT']) !!}
                    @include('forms.baseForm')
                        <div class='form-group'>
                            <div class='col-md-10'>
                                <button type='submit' class='btn btn-primary'>
                                    <i class='glyphicon glyphicon-save'></i> Guardar
                                </button>
                            </div>
                        </div>
                        <div class='form-group'>
                            <div class='col-md-1'>
                                <a href="{{route('forms.index')}}" class='btn btn-primary' style='margin-left:10px'>
                                    <i class='glyphicon glyphicon-remove-sign'></i>Cancelar
                                </a>
                            </div>
                        </div>
                    {!!Form::close()!!}
                </div>
            </div> 
    </div>
</div>
@endsection