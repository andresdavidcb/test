@extends('layouts.app')
@section('content')
@include('layouts.admin')
<meta charset='UTF-8'>
<div class='success'></div>
<div class='container col-md-10'>

	<div class='col-md-3'>
	<a href="{{ route('forms.create') }}" ><span class='glyphicon glyphicon-plus'></span>Creación de formulario</a>
	</div>
	
	<table class='table' id='myTable'>
		<thead><tr>
		<th>Menú</th><th>Ruta</th><th>Modelo</th><th>Etiqueta</th><th>Acciones</th>	
		</tr></thead>

		<tbody>
			@foreach($forms as $form)<tr>
			<td>{{ $form->module }}</td>
			<td>{{ $form->route }}</td>
			<td>{{ $form->model }}</td>
			<td>{{ $form->table_label }}</td>
			<td>
			<a href="{{ route('forms.edit',$form->id) }}" title='Modificar'><span class='glyphicon glyphicon-pencil btn btn-primary' style='padding:1px 3px'></span></a>
			<a href="{{ url('forms/anulate',$form->id) }}" title='Anular'><span class='glyphicon glyphicon-remove btn btn-primary' style='padding:1px 3px'></span></a>
			</td>
			
			</tr>
			@endforeach
		</tbody>		
	</table>
	
</div>	
@endsection