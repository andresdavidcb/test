@extends('layouts.app')

@section('content')
<div class='col-md-8'>      
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
      </ol>

      <!-- Wrapper for slides -->
      <div class="carousel-inner">
        <div class="item active">
          <img src="img/gallery/n1.jpg" alt="imagen">
        </div>

        <div class="item">
          <img src="img/gallery/n2.jpg" alt="imagen">
        </div>

        <div class="item">
          <img src="img/gallery/n3.jpg" alt="imagen">
        </div>
      </div>

      <!-- Left and right controls -->
      <a class="left carousel-control" href="#myCarousel" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="right carousel-control" href="#myCarousel" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>
        
</div>
<div class="col-md-4">
  <p>Brindamos Clases de
  Canto, Guitarra, Piano, Saxofón, Batería y bajo para todas las edades
  de forma personalizada.
</p>
<a href="{{ url('/signup') }}"><img src="img/gallery/backgroundFlyers.jpg" width="100%">
</a>
</div>


@endsection
