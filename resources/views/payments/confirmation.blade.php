
@extends('layouts.app')
@section('content')
<?php 
    $num= $payment->value;
    $newDate=$payment->created_at;

?>
<div id="main" class="container col-md-12">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-default">
                <div class="panel-heading">

                    
                    
                     <h2>Confirmación de pago</h2>
                </div>

                <div class="panel-body">
                    <table class='table'>
                        <thead>
                            <tr><th colspan="2"><?php echo ($payment['description'])?></th></tr>
                        </thead>
                        <tbody>
                            <tr><td>Fecha de emisión:</td><td><?php echo  date('d/m/Y',strtotime($newDate))?></td></tr>
                            <tr><td>Nombre del estudiante:</td><td><?php echo ($person['name']." ".$person['lastname'])?></td></tr>
                            <tr><td>Email:</td><td><?php echo ($person['email'])?></td></tr>
                            <tr><td>Valor a pagar:</td><td>$<?php echo number_format($num, 0, '', '.')?></td></tr>
                            <tr><td colspan="2">
                            <form method="post" action="https://gateway.payulatam.com/ppp-web-gateway/pb.zul" accept-charset="UTF-8">
                              <input type="image" border="0" alt="" src="http://www.payulatam.com/img-secure-2015/boton_pagar_pequeno.png" onClick="this.form.urlOrigen.value = window.location.href;"/>
                              <input name="buttonId" type="hidden" value="<?php echo $paymentDetail->buttonId?>"/>
                              <input name="merchantId" type="hidden" value="706607"/>
                              <input name="accountId" type="hidden" value="709733"/>
                              <input name="description" type="hidden" value="<?php echo $paymentDetail->description?>"/>
                              <input name="referenceCode" type="hidden" value="<?php echo $paymentDetail->referenceCode?>"/>
                              <input name="amount" type="hidden" value="<?php echo $paymentDetail->amount?>"/>
                              <input name="tax" type="hidden" value="0.00"/>
                              <input name="taxReturnBase" type="hidden" value="0.00"/>
                              <input name="currency" type="hidden" value="COP"/>
                              <input name="lng" type="hidden" value="es"/>
                              <input name="approvedResponseUrl" type="hidden" value="http://www.oasisacademia.com/paymentSuccess"/>
                              <input name="declinedResponseUrl" type="hidden" value="http://www.oasisacademia.com/paymentRejected"/>
                              <input name="pendingResponseUrl" type="hidden" value="http://www.oasisacademia.com/paymentProcessed"/>
                              <input name="paymentMethods" type="hidden" value="<?php echo $paymentDetail->paymentMethods?>"/>
                              <input name="sourceUrl" id="urlOrigen" value="" type="hidden"/>
                              <input name="buttonType" value="SIMPLE" type="hidden"/>
                              <input name="signature" value="<?php echo $paymentDetail->signature?>" type="hidden"/>
                            </form>
                            </td></tr>
                            
                        </tbody>        
                    </table>
                    </div>
                </div>



        </div>
    </div>


</div>

@endsection