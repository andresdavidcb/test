@extends('layouts.app')
@section('content')
@include('layouts.admin')
<div class='success'></div>
<div class='container col-md-10'>

    <div class='col-md-3'>
    <a href="{{ route('enrolments.create') }}" ><span class='glyphicon glyphicon-plus'></span>Creación de Matrícula</a>
    </div>
    <a href="{{ url('enrolments/pdf') }}"><i class='fa fa-file-pdf-o' aria-hidden='true'></i> Lista de usuarios</a>
    
    <table class='table' id='myTable'>
        <thead><tr>
        <th>Matricula No.</th><th>Nombre</th><th>Apellido</th><th>Identificación</th><th>Curso</th><th>Estado</th><th>Acciones</th>  
        </tr></thead>

        <tbody>
            @foreach($enrolment as $field)<tr>
           <td>{{ $field->registration }}</td>
           <td>{{ $field->firstname }}</td>
           <td>{{ $field->lastname }}</td>
           <td>{{ $field->identification }}</td>
           <td>{{ $field->course }}</td>
           <td>{{ $field->payment_status }}</td>
           <td>
            <a href="{{ route('enrolments.edit',$enrolment->id) }}" title='Modificar'><span class='glyphicon glyphicon-pencil btn btn-primary' style='padding:1px 3px'></span></a>
            <a href="{{ url('enrolments/anulate',$enrolment->id) }}" title='Anular'><span class='glyphicon glyphicon-remove btn btn-primary' style='padding:1px 3px'></span></a>
            </td>
            
            </tr>
            @endforeach
        </tbody>        
    </table>
    <p><strong>Total registros activos: </strong>{{$total}}</p>
</div>  
@endsection