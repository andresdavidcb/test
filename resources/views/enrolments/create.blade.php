@extends('layouts.app')
@section('content')
@include('layouts.admin')
<div class='container col-md-10'>
    <div class='row'>
            <div class='panel panel-default'>
                <div class='panel-heading'>Creación de Matrícula</div>
                <div class='panel-body'>
                    {!! Form::open(['route' => 'enrolments.store']) !!}
                        @include('enrolments.baseForm')
                        <div class='form-group'>
                            <div class='col-md-10'>
                                <button type='submit' id='btn_enrolment' class='btn btn-primary'><i class='glyphicon glyphicon-save'></i> Registrar</button>
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
    </div>
</div>
@endsection