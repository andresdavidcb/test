@extends('layouts.app')
@section('content')
@include('layouts.admin')
    <table width='100%'>
        <thead><tr>
        <th>Matricula No.</th><th>Nombre</th><th>Apellido</th><th>Identificación</th><th>Curso</th><th>Estado</th><th>Acciones</th>  
        </tr></thead>

        <tbody>
            @foreach($enrolment as $field)<tr>
           <td>{{ $field->registration }}</td>
           <td>{{ $field->firstname }}</td>
           <td>{{ $field->lastname }}</td>
           <td>{{ $field->identification }}</td>
           <td>{{ $field->course }}</td>
           <td>{{ $field->payment_status }}</td>
            
            </tr>
            @endforeach
        </tbody>        
    </table>
    <p><strong>Total registros activos: </strong>{{$total}}</p>
</div>  
@endsection