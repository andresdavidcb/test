<h2>DATOS PERSONALES</h2>
    <table class='table' id='myTable'>

        <tbody>
            @foreach($student as $field)<tr>
            
           <tr><th>Nombre completo:</td><td>{{ $field->name }} {{ $field->lastname }}</td></tr>
           <tr><th>Identificación:</th><td>{{ $field->id_type }} No. {{ $field->identification }}</td></tr>
           <tr><th>Teléfono principal</th><td>{{ $field->main_phone }}</td></tr>
           <tr><th>Teléfono secundario</th><td>{{ $field->sec_phone }}</td></tr>
           <tr><th>Correo electrónico</th><td>{{ $field->email }}</td></tr>
           <tr><th>Cursos de interés</th><td>{{ $field->interest_courses }}</td></tr>
           
            
            </tr>
            @endforeach
        </tbody>        
    </table>



<div class="form-group{{ $errors->has('identification') ? ' has-error' : '' }}">
    <label for='identification' class='col-md-3 control-label'>Identificación</label>
    <div class='col-md-9'>
    <input type='text' id='identification' name='identification' class='form-control' value="{{isset($enrolment)?$enrolment->identification:old('identification')}}">
     @if ($errors->has('identification'))
        <span class='help-block'><strong>{{ $errors->first('identification') }}</strong></span>
    @endif
    </div>
</div>

<div class="form-group{{ $errors->has('course') ? ' has-error' : '' }}">
    <label for='course' class='col-md-3 control-label'>Curso</label>
    <div class='col-md-9'>
   <select name='course' class='form-control'>
            @foreach($course as $field)
            @if((isset($enrolment)&&$enrolment->course==$field->id)||$field->id==old('course'))
            <option value="{{$field->id}}" selected>
            @else
            <option value="{{$field->id}}">
            @endif   
                {{$field->name}}</option>
            @endforeach
       </select>
            @if ($errors->has('course'))
        <span class='help-block'><strong>{{ $errors->first('course') }}</strong></span>
    @endif
    </div>
</div>

