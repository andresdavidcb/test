@extends('layouts.app')
@section('content')
@include('layouts.admin')
<div class='container col-md-10'>
    <div class='row'>
            <div class='panel panel-default'>
                <div class='panel-heading'><b>Edición de Matrícula</b> <span class='glyphicon glyphicon-arrow-right'></span> {{$enrolment->name}}</div>
                <div class='panel-body'>
                    {!! Form::model($enrolment,['route' => ['enrolments.update',$enrolment->id],'method'=>'PUT']) !!}
                    @include('enrolments.baseForm')
                        <div class='form-group'>
                            <div class='col-md-10'>
                                <button type='submit' class='btn btn-primary'>
                                    <i class='glyphicon glyphicon-save'></i> Guardar
                                </button>
                            </div>
                        </div>
                        <div class='form-group'>
                            <div class='col-md-1'>
                                <a href="{{route('enrolments.index')}}" class='btn btn-primary' style='margin-left:10px'>
                                    <i class='glyphicon glyphicon-remove-sign'></i>Cancelar
                                </a>
                            </div>
                        </div>
                    {!!Form::close()!!}
                </div>
            </div> 
    </div>
</div>
@endsection