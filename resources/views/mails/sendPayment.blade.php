<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8" />
        <title>ACADEMIA DE MÚSICA OASIS</title>
	<style>
	#container{background: url(http://www.oasisacademia.com/img/style/email.jpg) no-repeat center;padding:2% 15%;text-align: center;font-family: sans-serif;}
    #container h1,#container h2{color: #fff}
    #container p{font-size: 1.2em}
    #container a{background: #900;color:#fff;padding: 10px;border-radius: 3px;text-decoration: none}
    #container hr{border: #fff solid 2px}
   
    
	</style>

    </head>
    <body>
        
        <div id="container">
        	<header>
        		<img src="http://www.oasisacademia.com/img/style/oasis_music_logo.png" height="200px">
        	</header>
		    <hr>
            <h1>Hola, {!!$data['name']!!}!</h1>
            <P>¡Gracias por hacer parte de nuestra comunidad!</P>
            <p>Ahora en OASIS MUSIC hacemos todo más fácil para ti. 
            <p>Puedes pagar nuestros cursos con cualquier medio de pago</p>
            <a href="http://www.oasisacademia.com/paymentConfirmation?transaction={!!$data['transaction']!!}">
            Paga aquí</a>
            <h2>¿Tienes alguna inquietud?</h2>
            <p>Comunícate con nosotros:</p>
            <h3>320 747 3072</h3>

            <p>Carrera 14C # 48-42, Monteverde - Montería</p>
            <a href="https://www.google.com/maps/place/Oasis+Academia+de+M%C3%BAsica/@8.759845,-75.8705585,16z/data=!4m5!3m4!1s0x0:0xbfb899db9155c13d!8m2!3d8.7599514!4d-75.8675331?hl=es-ES">
            Ver mapa</a>
        </div>

    </body>
</html>