@extends('layouts.app')
@section('content')
<h1>ENVIAR SOLICITUD DE PAGO</h1>
<div class='container col-md-12' id="main">
{!! Form::open(['route' => 'paymentRequest.store','method' => 'POST']) !!}	
<!--<form method="post" v-on:submit.prevent="createItem">-->
<div class="col-md-6 form-group{{ $errors->has('paymentDetails') ? ' has-error' : '' }}">
	    <label for='paymentDetails' class='col-md-12 control-label'>Botón de pago</label>
	    <div class='col-md-12'>
	   <select name='paymentDetails' class='form-control'>
	            @foreach($paymentDetails as $field)
	            @if($field->id==old('paymentDetails'))
	            <option value="{{$field->id}}" selected>
	            @else
	            <option value="{{$field->id}}">
	            @endif   
	                {{$field->referenceCode}} - {{$field->description}} ( ${{$field->amount}} ) </option>
	            @endforeach
	       </select>
	          @if ($errors->has('paymentDetails'))
	        <span class='help-block'><strong>{{ $errors->first('paymentDetails') }}</strong></span>
	    @endif
	    </div>
	</div>

	<div class="col-md-6 form-group{{ $errors->has('email') ? ' has-error' : '' }}">
	    <label for='email' class='col-md-12 control-label'>Correo*</label>
	    <div class='col-md-12'>
	    <input required type='text' id='email' name='email' class='form-control' >
	     @if ($errors->has('email'))
	        <span class='help-block'><strong>{{ $errors->first('email') }}</strong></span>
	    @endif
	    </div>
	</div>

	
	

	
<div class='col-md-12' style="text-align:center">
<button type='submit' class='btn btn-danger'>
    Enviar solicitud <i class='glyphicon glyphicon-pencil'></i>
</button>


<br><br>
</div>



<div class='success'>
	{{Session::get('msg_signup')}}
	{{Session::forget('msg_signup')}}
</div>
{!! Form::close() !!}
<!--</form>-->


</div>
<!--
<script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.5.17/vue.js" ></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.18.0/axios.min.js" ></script>
<script src="{{asset('js/signup.js')}}"></script>
-->
@endsection