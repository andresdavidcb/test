@extends('layouts.app')
@section('content')
@include('layouts.admin')
    <table width='100%'>
        <thead><tr>
        <th>Apellido</th><th>Nombre</th><th>Teléfono principal</th><th>Teléfono secundario</th><th>Correo electrónico</th><th>Cursos de interes</th><th>Acciones</th>  
        </tr></thead>

        <tbody>
            @foreach($student as $field)<tr>
           <td>{{ $field->lastname }}</td>
           <td>{{ $field->name }}</td>
           <td>{{ $field->main_phone }}</td>
           <td>{{ $field->sec_phone }}</td>
           <td>{{ $field->email }}</td>
           <td>{{ $field->interest_courses }}</td>
            
            </tr>
            @endforeach
        </tbody>        
    </table>
    <p><strong>Total registros activos: </strong>{{$total}}</p>
</div>  
@endsection