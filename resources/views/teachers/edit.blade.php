
<div class="modal fade" id="edit">
<form method="POST" v-on:submit.prevent="updateItem(e.person)">    
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span>&times;</span>
                </button>
                <h4>Editar estudiante</h4>
            </div>
            <div class="modal-body">
                    <div class="col-md-6">
                        <label for='name' class='control-label'>Nombres*</label>
                        <input required type='text' name='name' v-model='e.name' class='form-control'>
                    </div>
                    
                    <div class="col-md-6">
                        <label for='lastname' class='control-label'>Apellidos*</label>
                        <input type='text' name='lastname' v-model='e.lastname' class='form-control'>
                    </div>

                    <div class="col-md-6">
                        <label for='id_type' class='control-label'>Tipo de documento*</label>
                        <select v-model="e.id_type" class="form-control" name='id_type' > 
                            <template v-for="item in id_types">
                                <option v-bind:value="item.id">
                                    @{{item.name}}
                                </option>
                            </template>
                            
                        </select>
                    </div>

                    <div class="col-md-6">
                        <label for='identification' class='control-label'>Número de documento*</label>
                        <input type='number' name='identification' v-model='e.identification' class='form-control'>
                    </div>

                    <div class="col-md-6">
                        <label for='birthdate' class='control-label'>Fecha de nacimiento*</label>
                        <input type='date' name='birthdate' v-model='e.birthdate' class='form-control'>
                    </div>

                    <div class="col-md-6">
                        <label for='email' class='control-label'>Correo electrónico*</label>
                        <input type='text' name='email' v-model='e.email' class='form-control' placeholder="example@mail.com">
                    </div>

                    <div class="col-md-6">
                        <label for='main_phone' class='control-label'>Teléfono principal*</label>
                        <input type='number' name='main_phone' v-model='e.main_phone' class='form-control'>
                    </div>
                    <div class="col-md-6">
                        <label for='sec_phone' class='control-label'>Teléfono alternativo</label>
                        <input type='number' name='sec_phone' v-model='e.sec_phone' class='form-control'>
                    </div>
                    <div class="col-md-12">
                        <label for='observations' class='control-label'>Observaciones</label>
                        <textarea name='observations' v-model='e.observations' class='form-control'>
                            @{{e.observations}}
                        </textarea>    
                    </div>
                    
            </div>
            <div class="modal-footer">
                <div class="col-md-6 col-md-offset-3">
                <input type="submit" class="btn btn-primary form-control" style="margin-top:10px" value="Guardar">
                </div>
            </div>
            
        </div>
        
    </div>   
</form>
</div>