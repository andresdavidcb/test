@extends('layouts.app')
@section('content')
@include('layouts.admin')



<div id="main" class="container col-md-10">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">

                    <div class="col-md-4">
                    <input type="search" v-model="search" placeholder="Buscar" class="form-control">
                    </div>
                    
                     <a href="#" class="btn btn-primary" v-on:click="searchItems(search)"><i class="glyphicon glyphicon-search"></i></a>
                    </form>
                    <a href="#" class="btn btn-primary pull-right" data-toggle="modal" data-target="#create">
                        <i class="glyphicon glyphicon-plus"></i>
                    </a>
                </div>

                <div class="panel-body">
                    <table class='table'>
        <thead><tr>
        <th>Acciones</th>
        <th>Nombre completo</th>
        <th>Teléfono principal</th>
         
        <th>Estado</th>  
        </tr></thead>
        <tbody>
            <tr v-for="item in list">
            <td>
            <a href="#" title='Ver detalles' v-on:click="showItem(item)"><span class='glyphicon glyphicon-eye-open btn btn-success' style='padding:1px 3px'></span></a>
            <a href="#" title='Modificar' v-on:click="editItem(item)"><span class='glyphicon glyphicon-pencil btn btn-primary' style='padding:1px 3px'></span></a>
            <a href="#" title='Matrícula' v-on:click="enrolment(item)"><span class='glyphicon glyphicon-list-alt btn btn-info' style='padding:1px 3px'></span></a>
            </td>
           <td>@{{ item.name }} @{{ item.lastname }}</td>
           <td>@{{ item.main_phone }}</td>
           <td>@{{ item.status }}</td>
            </tr>
            
        </tbody>        
    </table>
    </div>
</div><!--endPannel-->

            @include('teachers.create')
            @include('teachers.show')
            @include('teachers.edit')



        </div>
    </div>


    <pre>
        @{{$data}}
    </pre>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.5.17/vue.js" ></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.18.0/axios.min.js" ></script>
<script src="{{asset('js/teachers.js')}}"></script>
@endsection