@extends('layouts.app')
@section('content')

<div class='container'>
	<div class='col-md-6 imgAcordeon'>
		<h2>GUITARRA ACÚSTICA / ELECTRICA</h2>
		<div class='Img'>
			<img src="img/gallery/i1.jpg">
		</div>

		<div class='acord'>
			<h3>Unidad # 1: Básico</h3>
			<ul>
				<li>Instrumento I</li>
				<li>Gramática I</li>
				<li>Ejercicios y posiciones</li>
				<li>Armonía I aplicada a la guitarra</li>
				<li>Práctica Instrumental I</li>
			</ul>
			<h3>Unidad # 2: Medio</h3>
			<ul>
				<li>Gramática II</li>
				<li>Ejercicios y posiciones II(Canciones)</li>
				<li>Armonía II</li>
				<li>Práctica Instrumental II</li>
				<li>Oratoria</li>
			</ul>
			<h3>Unidad # 3: Avanzado</h3>
			<ul>
				<li>Gramática III</li>
				<li>Ensamble</li>
				<li>Armonía III</li>
				<li>Orquestación</li>
				<li>Psicología musical</li>
			</ul>
		</div>
	</div>


	<div class='col-md-6 imgAcordeon'>
		<h2>BAJO</h2>
		<div class='Img'>
			<img src="img/gallery/i2.jpg">
		</div>
		<div class='acord'>
			<h3>Unidad # 1: Básico</h3>
			<ul>
				<li>Instrumento I</li>
				<li>Gramática</li>
				<li>Ejercicios y posiciones (patrones rítmicos)</li>
				<li>Armonía aplicada al bajo</li>
				<li>Práctica Instrumental I</li>
			</ul>

			<h3>Unidad # 2: Medio</h3>
			<ul>
				<li>Gramática II</li>
				<li>Ritmos y técnicas aplicadas al bajo</li>
				<li>Armonía II</li>
				<li>Práctica Instrumental II</li>
				<li>Oratoria</li>
			</ul>
			<h3>Unidad # 3: Avanzado</h3>
			<ul>
				<li>Gramática III</li>
				<li>Ensamble grupal</li>
				<li>Armonía III</li>
				<li>Orquestación</li>
				<li>Psicología musical</li>
			</ul>
		</div>

	</div>

	<div class='col-md-6 imgAcordeon'>
		<h2>PIANO</h2>
		<div class='Img'>
			<img src="img/gallery/i3.jpg">
		</div>
		<div class='acord'>
			
			<h3>Unidad # 1: Básico</h3>
			<ul>
				<li>Instrumento I</li>
				<li>Gramática</li>
				<li>Ejercicios y técnicas en ambas manos</li>
				<li>Pocisión de las manos</li>
				<li>Gramática I</li>
				<li>Armonía I</li>
				<li>Práctica Instrumental I</li>
			</ul>

			<h3>Unidad # 2: Medio</h3>
			<ul>
				<li>Concepto de Escalas</li>
				<li>Ejercicio de Escalas</li>
				<li>Gramática II</li>
				<li>Armonía II</li>
				<li>Práctica Instrumental II</li>
				<li>Oratoria</li>
			</ul>
			<h3>Unidad # 3: Avanzado</h3>
			<ul>
				<li>Gramática III</li>
				<li>Ensamble grupal</li>
				<li>Armonía III</li>
				<li>Orquestación</li>
				<li>Psicología musical</li>
			</ul>
		</div>

	</div>


	<div class='col-md-6 imgAcordeon'>
		<h2>BATERÍA</h2>
		<div class='Img'>
			<img src="img/gallery/i4.jpg">
		</div>
		<div class='acord'>		
			<h3>Unidad # 1: Básico</h3>
			<ul>
				<li>Identificación de los elementos de la batería</li>
				<li>Lenguaje Musical I</li>
				<li>Técnica de redoblante</li>
				<li>Ejercicios de lectura rítmica</li>
				<li>Performance de la batería</li>
				<li>Práctica Instrumental I</li>
			</ul>

			<h3>Unidad # 2: Medio</h3>
			<ul>
				<li>Lenguaje Musical II</li>
				<li>Ejercicios de Lectura Ritmica II</li>
				<li>Ritmos musicales en batería</li>
				<li>Práctica Instrumental II</li>
				<li>Oratoria</li>
			</ul>
			<h3>Unidad # 3: Avanzado</h3>
			<ul>
				<li>Ejecución de la batería en ritmos latinos</li>
				<li>Improvisación</li>
				<li>Módulo complementario de ritmos</li>
				<li>Practica Instrumental III</li>
				<li>Psicología musical</li>
			</ul>
		</div>		
	</div>

	<div class='col-md-6 imgAcordeon'>
		<h2>VOCALIZACIÓN</h2>
		<div class='Img'>
			<img src="img/gallery/i5.jpg">
		</div>
		<div class='acord'>		
			<h3>Unidad # 1: Básico</h3>
			<ul>
				<li>Técnicas y ejercicios aplicadas al canto</li>
				<li>Introducción a la escala musical</li>
				<li>Grmática I</li>
				<li>Preparación al solfeo</li>
				<li>Práctica Grupal I (Instrumental)</li>
			</ul>

			<h3>Unidad # 2: Medio</h3>
			<ul>
				<li>Técnicas y ejercicios aplicadas al canto II</li>
				<li>Grmática II</li>
				<li>Solfeo</li>
				<li>Práctica Grupal II</li>
				<li>Oratoria</li>
			</ul>
			<h3>Unidad # 3: Avanzado</h3>
			<ul>
				<li>Gramática III</li>
				<li>Ensamble coral</li>
				<li>Práctica grupal III</li>
				<li>Psicología musical</li>
			</ul>
		</div>		
	</div>


	<div class='col-md-6 imgAcordeon'>
		<h2>SAXOFÓN</h2>
		<div class='Img'>
			<img src="img/gallery/i6.jpg">
		</div>
		<div class='acord'>		
			<h3>Unidad # 1: Básico</h3>
			<ul>
				<li>Introducción al saxofón</li>
				<li>Gramática I</li>
				<li>Ejercicios y Escalas I</li>
				<li>Armonía aplicada al saxofón</li>
				<li>Práctica Instrumental I</li>
			</ul>

			<h3>Unidad # 2: Medio</h3>
			<ul>
				<li>Gramática II</li>
				<li>Ejercicios y escalas II</li>
				<li>Armonía II</li>
				<li>Práctica Instrumental II</li>
				<li>Oratoria</li>
			</ul>
			<h3>Unidad # 3: Avanzado</h3>
			<ul>
				<li>Gramática III</li>
				<li>Armonía III</li>
				<li>Ensamble grupal</li>
				<li>Psicología musical</li>
				<li>Orquestación</li>
			</ul>
		</div>		
	</div>




</div>


		

@endsection