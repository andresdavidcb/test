@extends('layouts.app')
@section('content')

<div class='container col-md-12'>
<div class='col-md-8'>	
<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15773.242386496562!2d-75.8772967!3d8.7568192!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xbbb9ad900a95d41b!2sOasis+Academia+de+M%C3%BAsica!5e0!3m2!1ses!2sco!4v1473475229321" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>

<div class='col-md-4'>
{!! Form::open(['route' => 'contact.store','method' => 'POST']) !!}	
	<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
	    <label for='name' class='col-md-12 control-label'>Nombre</label>
	    <div class='col-md-12'>
	    <input required type='text' id='name' name='name' class='form-control' value="{{old('name')}}">
	     @if ($errors->has('name'))
	        <span class='help-block'><strong>{{ $errors->first('name') }}</strong></span>
	    @endif
	    </div>
	</div>

	<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
	    <label for='email' class='col-md-12 control-label'>Correo electrónico</label>
	    <div class='col-md-12'>
	    <input required type='email' id='email' name='email' class='form-control' value="{{old('email')}}">
	     @if ($errors->has('email'))
	        <span class='help-block'><strong>{{ $errors->first('email') }}</strong></span>
	    @endif
	    </div>
	</div>

	<div class="form-group{{ $errors->has('msg') ? ' has-error' : '' }}">
	    <label for='msg' class='col-md-12 control-label'>Mensaje</label>
	    <div class='col-md-12'>
	    <textarea required  id='msg' name='msg' class='form-control' rows="7">{{old('msg')}}</textarea>
	     @if ($errors->has('msg'))
	        <span class='help-block'><strong>{{ $errors->first('msg') }}</strong></span>
	    @endif
	    </div>
	</div>

	<div class="col-md-12">
		<button class='btn-danger' style="width:100%;margin:10px 0">Comentar</button>
		Cel: 320 747 3072<br>
		Carrera 14C # 48-42 Apto 02, Monteverde<br>
		Montería - Córdoba<br>
		contacto@oasisacademia.com<br>
	</div>
	<div id='message' class='successs'>
	</div>


{!!Form::close()!!}
</div>


</div>


@endsection