<div class="modal fade" id="show">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span>&times;</span>
                </button>
                <h4>Datos personales</h4>
            </div>
            <div class="modal-body">
                    <table class="table table-hover">
                        <tbody>
                        <tr><th>Nombre completo:</th><td>@{{s.fullname}}</td></tr>
                        <tr><th>Tipo de documento:</th><td>@{{s.id_type}}</td></tr>
                        <tr><th>Número de documento:</th><td>@{{s.identification}}</td></tr>
                        <tr><th>Fecha de nacimiento:</th><td>@{{s.birthdate}}</td></tr>
                        <tr><th>Teléfono principal:</th><td>@{{s.main_phone}}</td></tr>
                        <tr><th>Teléfono alternativo:</th><td>@{{s.sec_phone}}</td></tr>
                        <tr><th>Email:</th><td>@{{s.email}}</td></tr>
                        <tr><th>Observations:</th><td>@{{s.observations}}</td></tr>
                    </tbody>
                       
                        
                    </table>
            </div>
            <div class="modal-footer">
                
            </div>
            
        </div>
        
    </div>
    
</div>


