@extends('layouts.app')
@section('content')
@include('layouts.admin')
<div class='success'></div>
<div class='container col-md-10'>

    <div class='col-md-3'>
    <a href="{{ route('students.create') }}" ><span class='glyphicon glyphicon-plus'></span>Creación de Estudiante</a>
    </div>
    <a href="{{ url('students/pdf') }}"><i class='fa fa-file-pdf-o' aria-hidden='true'></i> Lista de usuarios</a>
    <h2>DATOS PERSONALES</h2>
    <table class='table' id='myTable'>

        <tbody>
            @foreach($student as $field)<tr>
            <?php
            $new = new DateTime($field->created_date);
            $pre_date=$new->format('d/m/Y \a \l\a\s h:m a');
            ?>
           <tr><th>Fecha de preinscripción:</td><td>{{ $pre_date }}</td></tr>
           <tr><th>Nombre completo:</td><td>{{ $field->name }} {{ $field->lastname }}</td></tr>
           <tr><th>Identificación:</th><td>{{ $field->id_type }} No. {{ $field->identification }}</td></tr>
           <tr><th>Teléfono principal</th><td>{{ $field->main_phone }}</td></tr>
           <tr><th>Teléfono secundario</th><td>{{ $field->sec_phone }}</td></tr>
           <tr><th>Correo electrónico</th><td>{{ $field->email }}</td></tr>
           <tr><th>Cursos de interés</th><td>{{ $field->interest_courses }}</td></tr>
           <tr><th>Observaciones</th><td>{{ $field->observations }}</td></tr>
           
            
            </tr>
            @endforeach
        </tbody>        
    </table>
    
</div>  
@endsection