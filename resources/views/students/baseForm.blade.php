<div class="col-md-6 form-group{{ $errors->has('name') ? ' has-error' : '' }}">
        <label for='name' class='col-md-12 control-label'>Nombres*</label>
        <div class='col-md-12'>
        <input required type='text' id='name' name='name' class='form-control' value="{{isset($student)?$student->name:old('name')}}">
         @if ($errors->has('name'))
            <span class='help-block'><strong>{{ $errors->first('name') }}</strong></span>
        @endif
        </div>
    </div>

    <div class="col-md-6 form-group{{ $errors->has('lastname') ? ' has-error' : '' }}">
        <label for='lastname' class='col-md-12 control-label'>Apellidos*</label>
        <div class='col-md-12'>
        <input required type='text' id='lastname' name='lastname' class='form-control' value="{{isset($student)?$student->lastname:old('lastname')}}">
         @if ($errors->has('lastname'))
            <span class='help-block'><strong>{{ $errors->first('lastname') }}</strong></span>
        @endif
        </div>
    </div>

    <div class="col-md-6 form-group{{ $errors->has('id_type') ? ' has-error' : '' }}">
        <label for='id_type' class='col-md-12 control-label'>Tipo de documento</label>
        <div class='col-md-12'>
       <select name='id_type' class='form-control'>
                @foreach($id_type as $field)
                @if((isset($student)&&$student->id_type==$field->id)||$field->id==old('id_type'))
                <option value="{{$field->id}}" selected>
                @else
                <option value="{{$field->id}}">
                @endif   
                    {{$field->name}}</option>
                @endforeach
           </select>
              @if ($errors->has('id_type'))
            <span class='help-block'><strong>{{ $errors->first('id_type') }}</strong></span>
        @endif
        </div>
    </div>


    <div class="col-md-6 form-group{{ $errors->has('identification') ? ' has-error' : '' }}">
        <label for='identification' class='col-md-12 control-label'>Identificación*</label>
        <div class='col-md-12'>
        <input required type='text' id='identification' name='identification' class='form-control' value="{{isset($student)?$student->identification:old('identification')}}">
         @if ($errors->has('identification'))
            <span class='help-block'><strong>{{ $errors->first('identification') }}</strong></span>
        @endif
        </div>
    </div>

    <div class="col-md-6 form-group{{ $errors->has('birthdate') ? ' has-error' : '' }}">
        <label for='birthdate' class='col-md-12 control-label'>Fecha de nacimiento</label>
        <div class='col-md-12'>
        <input type='date' id='birthdate' name='birthdate' class='form-control' value="{{isset($student)?$student->birthdate:old('birthdate')}}">
         @if ($errors->has('birthdate'))
            <span class='help-block'><strong>{{ $errors->first('birthdate') }}</strong></span>
        @endif
        </div>
    </div>

    <div class="col-md-6 form-group{{ $errors->has('email') ? ' has-error' : '' }}">
        <label for='email' class='col-md-12 control-label'>Correo electrónico*</label>
        <div class='col-md-12'>
        <input required type='email' id='email' name='email' class='form-control' value="{{isset($student)?$student->email:old('email')}}">
         @if ($errors->has('email'))
            <span class='help-block'><strong>{{ $errors->first('email') }}</strong></span>
        @endif
        </div>
    </div>

    <div required class="col-md-6 form-group{{ $errors->has('main_phone') ? ' has-error' : '' }}">
        <label for='main_phone' class='col-md-12 control-label'>Teléfono principal*</label>
        <div class='col-md-12'>
        <input type='number' id='main_phone' name='main_phone' class='form-control' value="{{isset($student)?$student->main_phone:old('main_phone')}}">
         @if ($errors->has('main_phone'))
            <span class='help-block'><strong>{{ $errors->first('main_phone') }}</strong></span>
        @endif
        </div>
    </div>

    <div class="col-md-6 form-group{{ $errors->has('sec_phone') ? ' has-error' : '' }}">
        <label for='sec_phone' class='col-md-12 control-label'>Teléfono alternativo</label>
        <div class='col-md-12'>
        <input type='number' id='sec_phone' name='sec_phone' class='form-control' value="{{isset($student)?$student->sec_phone:old('sec_phone')}}">
         @if ($errors->has('sec_phone'))
            <span class='help-block'><strong>{{ $errors->first('sec_phone') }}</strong></span>
        @endif
        </div>
    </div>

    <div class="col-md-6 form-group{{ $errors->has('interest_courses') ? ' has-error' : '' }}">
        <label for='interest_courses' class='col-md-12 control-label'>Cursos de interes*</label>
        <div class='col-md-12'>

            @foreach($course_category as $field)
            <div class='col-md-4'>
                <input class='' name='interest_courses[]' type='checkbox' value='{{$field->name}}'>{{$field->name}}
            </div>
            @endforeach

            @if ($errors->has('interest_courses[]'))
                <span class='help-block'><strong>{{ $errors->first('interest_courses[]') }}</strong></span>
            @endif
        </div>       
    </div>

    <div class="col-md-6 form-group{{ $errors->has('observations') ? ' has-error' : '' }}">
        <label for='observations' class='col-md-12 control-label'>Observaciones</label>
        <div class='col-md-12'>
        <textarea id='observations' name='observations' class='form-control'>{{isset($student)?$student->observations:old('observations')}}</textarea>
         @if ($errors->has('observations'))
            <span class='help-block'><strong>{{ $errors->first('observations') }}</strong></span>
        @endif
        </div>
    </div>
    
</div>



